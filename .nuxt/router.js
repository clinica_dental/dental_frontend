import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6493e434 = () => interopDefault(import('../pages/autenticacion.vue' /* webpackChunkName: "pages/autenticacion" */))
const _36b86402 = () => interopDefault(import('../pages/inicio.vue' /* webpackChunkName: "pages/inicio" */))
const _34a6e7e5 = () => interopDefault(import('../pages/dental/antecedentes.vue' /* webpackChunkName: "pages/dental/antecedentes" */))
const _43d730a5 = () => interopDefault(import('../pages/dental/consulta.vue' /* webpackChunkName: "pages/dental/consulta" */))
const _2578a421 = () => interopDefault(import('../pages/dental/paciente.vue' /* webpackChunkName: "pages/dental/paciente" */))
const _41f9dc6d = () => interopDefault(import('../pages/dental/personal-odontologico.vue' /* webpackChunkName: "pages/dental/personal-odontologico" */))
const _f9ab4812 = () => interopDefault(import('../pages/dental/personas.vue' /* webpackChunkName: "pages/dental/personas" */))
const _49ee666b = () => interopDefault(import('../pages/dental/reservas.vue' /* webpackChunkName: "pages/dental/reservas" */))
const _00575e60 = () => interopDefault(import('../pages/dental/tratamiento.vue' /* webpackChunkName: "pages/dental/tratamiento" */))
const _26afa045 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/autenticacion",
    component: _6493e434,
    name: "autenticacion"
  }, {
    path: "/inicio",
    component: _36b86402,
    name: "inicio"
  }, {
    path: "/dental/antecedentes",
    component: _34a6e7e5,
    name: "dental-antecedentes"
  }, {
    path: "/dental/consulta",
    component: _43d730a5,
    name: "dental-consulta"
  }, {
    path: "/dental/paciente",
    component: _2578a421,
    name: "dental-paciente"
  }, {
    path: "/dental/personal-odontologico",
    component: _41f9dc6d,
    name: "dental-personal-odontologico"
  }, {
    path: "/dental/personas",
    component: _f9ab4812,
    name: "dental-personas"
  }, {
    path: "/dental/reservas",
    component: _49ee666b,
    name: "dental-reservas"
  }, {
    path: "/dental/tratamiento",
    component: _00575e60,
    name: "dental-tratamiento"
  }, {
    path: "/",
    component: _26afa045,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
