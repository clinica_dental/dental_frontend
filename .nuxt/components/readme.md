# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<NuxtLogo>` | `<nuxt-logo>` (components/NuxtLogo.vue)
- `<Tutorial>` | `<tutorial>` (components/Tutorial.vue)
- `<VuetifyLogo>` | `<vuetify-logo>` (components/VuetifyLogo.vue)
- `<DentalObtieneAntecedentes>` | `<dental-obtiene-antecedentes>` (components/dental/ObtieneAntecedentes.vue)
- `<DentalObtieneAntecedentesAfecciones>` | `<dental-obtiene-antecedentes-afecciones>` (components/dental/ObtieneAntecedentesAfecciones.vue)
- `<DentalObtieneAntecedentesDentales>` | `<dental-obtiene-antecedentes-dentales>` (components/dental/ObtieneAntecedentesDentales.vue)
- `<DentalObtieneAntecedentesPatologicos>` | `<dental-obtiene-antecedentes-patologicos>` (components/dental/ObtieneAntecedentesPatologicos.vue)
- `<DentalObtieneAntecedentesPersonales>` | `<dental-obtiene-antecedentes-personales>` (components/dental/ObtieneAntecedentesPersonales.vue)
- `<DentalObtieneConsulta>` | `<dental-obtiene-consulta>` (components/dental/ObtieneConsulta.vue)
- `<DentalObtienePaciente>` | `<dental-obtiene-paciente>` (components/dental/ObtienePaciente.vue)
- `<DentalObtienePersonalOdontologico>` | `<dental-obtiene-personal-odontologico>` (components/dental/ObtienePersonalOdontologico.vue)
- `<DentalObtienePersonas>` | `<dental-obtiene-personas>` (components/dental/ObtienePersonas.vue)
- `<DentalObtieneReservas>` | `<dental-obtiene-reservas>` (components/dental/ObtieneReservas.vue)
- `<DentalObtieneTratamientoPlan>` | `<dental-obtiene-tratamiento-plan>` (components/dental/ObtieneTratamientoPlan.vue)
- `<DentalObtieneTratamientos>` | `<dental-obtiene-tratamientos>` (components/dental/ObtieneTratamientos.vue)
- `<GlobalesCaptcha>` | `<globales-captcha>` (components/globales/Captcha.vue)
- `<GlobalesCardToolTip>` | `<globales-card-tool-tip>` (components/globales/CardToolTip.vue)
- `<GlobalesDateInputPicker>` | `<globales-date-input-picker>` (components/globales/DateInputPicker.vue)
- `<GlobalesFileVisor>` | `<globales-file-visor>` (components/globales/FileVisor.vue)
- `<GlobalesIconToolTip>` | `<globales-icon-tool-tip>` (components/globales/IconToolTip.vue)
- `<GlobalesMonthInputPicker>` | `<globales-month-input-picker>` (components/globales/MonthInputPicker.vue)
- `<GlobalesSpanExpandible>` | `<globales-span-expandible>` (components/globales/SpanExpandible.vue)
