export const NuxtLogo = () => import('../../components/NuxtLogo.vue' /* webpackChunkName: "components/nuxt-logo" */).then(c => wrapFunctional(c.default || c))
export const Tutorial = () => import('../../components/Tutorial.vue' /* webpackChunkName: "components/tutorial" */).then(c => wrapFunctional(c.default || c))
export const VuetifyLogo = () => import('../../components/VuetifyLogo.vue' /* webpackChunkName: "components/vuetify-logo" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneAntecedentes = () => import('../../components/dental/ObtieneAntecedentes.vue' /* webpackChunkName: "components/dental-obtiene-antecedentes" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneAntecedentesAfecciones = () => import('../../components/dental/ObtieneAntecedentesAfecciones.vue' /* webpackChunkName: "components/dental-obtiene-antecedentes-afecciones" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneAntecedentesDentales = () => import('../../components/dental/ObtieneAntecedentesDentales.vue' /* webpackChunkName: "components/dental-obtiene-antecedentes-dentales" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneAntecedentesPatologicos = () => import('../../components/dental/ObtieneAntecedentesPatologicos.vue' /* webpackChunkName: "components/dental-obtiene-antecedentes-patologicos" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneAntecedentesPersonales = () => import('../../components/dental/ObtieneAntecedentesPersonales.vue' /* webpackChunkName: "components/dental-obtiene-antecedentes-personales" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneConsulta = () => import('../../components/dental/ObtieneConsulta.vue' /* webpackChunkName: "components/dental-obtiene-consulta" */).then(c => wrapFunctional(c.default || c))
export const DentalObtienePaciente = () => import('../../components/dental/ObtienePaciente.vue' /* webpackChunkName: "components/dental-obtiene-paciente" */).then(c => wrapFunctional(c.default || c))
export const DentalObtienePersonalOdontologico = () => import('../../components/dental/ObtienePersonalOdontologico.vue' /* webpackChunkName: "components/dental-obtiene-personal-odontologico" */).then(c => wrapFunctional(c.default || c))
export const DentalObtienePersonas = () => import('../../components/dental/ObtienePersonas.vue' /* webpackChunkName: "components/dental-obtiene-personas" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneReservas = () => import('../../components/dental/ObtieneReservas.vue' /* webpackChunkName: "components/dental-obtiene-reservas" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneTratamientoPlan = () => import('../../components/dental/ObtieneTratamientoPlan.vue' /* webpackChunkName: "components/dental-obtiene-tratamiento-plan" */).then(c => wrapFunctional(c.default || c))
export const DentalObtieneTratamientos = () => import('../../components/dental/ObtieneTratamientos.vue' /* webpackChunkName: "components/dental-obtiene-tratamientos" */).then(c => wrapFunctional(c.default || c))
export const GlobalesCaptcha = () => import('../../components/globales/Captcha.vue' /* webpackChunkName: "components/globales-captcha" */).then(c => wrapFunctional(c.default || c))
export const GlobalesCardToolTip = () => import('../../components/globales/CardToolTip.vue' /* webpackChunkName: "components/globales-card-tool-tip" */).then(c => wrapFunctional(c.default || c))
export const GlobalesDateInputPicker = () => import('../../components/globales/DateInputPicker.vue' /* webpackChunkName: "components/globales-date-input-picker" */).then(c => wrapFunctional(c.default || c))
export const GlobalesFileVisor = () => import('../../components/globales/FileVisor.vue' /* webpackChunkName: "components/globales-file-visor" */).then(c => wrapFunctional(c.default || c))
export const GlobalesIconToolTip = () => import('../../components/globales/IconToolTip.vue' /* webpackChunkName: "components/globales-icon-tool-tip" */).then(c => wrapFunctional(c.default || c))
export const GlobalesMonthInputPicker = () => import('../../components/globales/MonthInputPicker.vue' /* webpackChunkName: "components/globales-month-input-picker" */).then(c => wrapFunctional(c.default || c))
export const GlobalesSpanExpandible = () => import('../../components/globales/SpanExpandible.vue' /* webpackChunkName: "components/globales-span-expandible" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
