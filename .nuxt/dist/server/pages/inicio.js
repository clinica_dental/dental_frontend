exports.ids = [25];
exports.modules = {

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(244);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("5db1c400", content, true)

/***/ }),

/***/ 244:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".theme--light.v-alert .v-alert--prominent .v-alert__icon:after{background:rgba(0,0,0,.12)}.theme--dark.v-alert .v-alert--prominent .v-alert__icon:after{background:hsla(0,0%,100%,.12)}.v-sheet.v-alert{border-radius:4px}.v-sheet.v-alert:not(.v-sheet--outlined){box-shadow:0 0 0 0 rgba(0,0,0,.2),0 0 0 0 rgba(0,0,0,.14),0 0 0 0 rgba(0,0,0,.12)}.v-sheet.v-alert.v-sheet--shaped{border-radius:24px 4px}.v-alert{display:block;font-size:16px;margin-bottom:16px;padding:16px;position:relative;transition:.3s cubic-bezier(.25,.8,.5,1)}.v-alert:not(.v-sheet--tile){border-radius:4px}.v-application--is-ltr .v-alert>.v-alert__content,.v-application--is-ltr .v-alert>.v-icon{margin-right:16px}.v-application--is-rtl .v-alert>.v-alert__content,.v-application--is-rtl .v-alert>.v-icon{margin-left:16px}.v-application--is-ltr .v-alert>.v-icon+.v-alert__content{margin-right:0}.v-application--is-rtl .v-alert>.v-icon+.v-alert__content{margin-left:0}.v-application--is-ltr .v-alert>.v-alert__content+.v-icon{margin-right:0}.v-application--is-rtl .v-alert>.v-alert__content+.v-icon{margin-left:0}.v-alert__border{border-style:solid;border-width:4px;content:\"\";position:absolute}.v-alert__border:not(.v-alert__border--has-color){opacity:.26}.v-alert__border--left,.v-alert__border--right{bottom:0;top:0}.v-alert__border--bottom,.v-alert__border--top{left:0;right:0}.v-alert__border--bottom{border-bottom-left-radius:inherit;border-bottom-right-radius:inherit;bottom:0}.v-application--is-ltr .v-alert__border--left{border-top-left-radius:inherit;border-bottom-left-radius:inherit;left:0}.v-application--is-ltr .v-alert__border--right,.v-application--is-rtl .v-alert__border--left{border-top-right-radius:inherit;border-bottom-right-radius:inherit;right:0}.v-application--is-rtl .v-alert__border--right{border-top-left-radius:inherit;border-bottom-left-radius:inherit;left:0}.v-alert__border--top{border-top-left-radius:inherit;border-top-right-radius:inherit;top:0}.v-alert__content{flex:1 1 auto}.v-application--is-ltr .v-alert__dismissible{margin:-16px -8px -16px 8px}.v-application--is-rtl .v-alert__dismissible{margin:-16px 8px -16px -8px}.v-alert__icon{align-self:flex-start;border-radius:50%;height:24px;min-width:24px;position:relative}.v-application--is-ltr .v-alert__icon{margin-right:16px}.v-application--is-rtl .v-alert__icon{margin-left:16px}.v-alert__icon.v-icon{font-size:24px}.v-alert__wrapper{align-items:center;border-radius:inherit;display:flex}.v-application--is-ltr .v-alert--border.v-alert--prominent .v-alert__icon{margin-left:8px}.v-application--is-rtl .v-alert--border.v-alert--prominent .v-alert__icon{margin-right:8px}.v-alert--dense{padding-top:8px;padding-bottom:8px}.v-alert--dense .v-alert__border{border-width:medium}.v-alert--outlined{background:transparent!important;border:thin solid!important}.v-alert--outlined .v-alert__icon{color:inherit!important}.v-alert--prominent .v-alert__icon{align-self:center;height:48px;min-width:48px}.v-alert--prominent .v-alert__icon.v-icon{font-size:32px}.v-alert--prominent .v-alert__icon.v-icon:after{background:currentColor!important;border-radius:50%;bottom:0;content:\"\";left:0;opacity:.16;position:absolute;right:0;top:0}.v-alert--prominent.v-alert--dense .v-alert__icon.v-icon:after{transform:scale(1)}.v-alert--text{background:transparent!important}.v-alert--text:before{background-color:currentColor;border-radius:inherit;bottom:0;content:\"\";left:0;opacity:.12;position:absolute;pointer-events:none;right:0;top:0}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VAlert/VAlert.sass
var VAlert = __webpack_require__(243);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/index.js
var VSheet = __webpack_require__(34);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/index.js
var VBtn = __webpack_require__(41);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/index.js
var VIcon = __webpack_require__(11);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/toggleable/index.js
var toggleable = __webpack_require__(17);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/themeable/index.js
var themeable = __webpack_require__(6);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(1);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/mixins/transitionable/index.js

/* harmony default export */ var transitionable = (external_vue_default.a.extend({
  name: 'transitionable',
  props: {
    mode: String,
    origin: String,
    transition: String
  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(2);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/console.js
var console = __webpack_require__(3);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VAlert/VAlert.js
// Styles
 // Extensions

 // Components


 // Mixins



 // Utilities



/* @vue/component */

/* harmony default export */ var VAlert_VAlert = __webpack_exports__["a"] = (Object(mixins["a" /* default */])(VSheet["a" /* default */], toggleable["a" /* default */], transitionable).extend({
  name: 'v-alert',
  props: {
    border: {
      type: String,

      validator(val) {
        return ['top', 'right', 'bottom', 'left'].includes(val);
      }

    },
    closeLabel: {
      type: String,
      default: '$vuetify.close'
    },
    coloredBorder: Boolean,
    dense: Boolean,
    dismissible: Boolean,
    closeIcon: {
      type: String,
      default: '$cancel'
    },
    icon: {
      default: '',
      type: [Boolean, String],

      validator(val) {
        return typeof val === 'string' || val === false;
      }

    },
    outlined: Boolean,
    prominent: Boolean,
    text: Boolean,
    type: {
      type: String,

      validator(val) {
        return ['info', 'error', 'success', 'warning'].includes(val);
      }

    },
    value: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    __cachedBorder() {
      if (!this.border) return null;
      let data = {
        staticClass: 'v-alert__border',
        class: {
          [`v-alert__border--${this.border}`]: true
        }
      };

      if (this.coloredBorder) {
        data = this.setBackgroundColor(this.computedColor, data);
        data.class['v-alert__border--has-color'] = true;
      }

      return this.$createElement('div', data);
    },

    __cachedDismissible() {
      if (!this.dismissible) return null;
      const color = this.iconColor;
      return this.$createElement(VBtn["a" /* default */], {
        staticClass: 'v-alert__dismissible',
        props: {
          color,
          icon: true,
          small: true
        },
        attrs: {
          'aria-label': this.$vuetify.lang.t(this.closeLabel)
        },
        on: {
          click: () => this.isActive = false
        }
      }, [this.$createElement(VIcon["a" /* default */], {
        props: {
          color
        }
      }, this.closeIcon)]);
    },

    __cachedIcon() {
      if (!this.computedIcon) return null;
      return this.$createElement(VIcon["a" /* default */], {
        staticClass: 'v-alert__icon',
        props: {
          color: this.iconColor
        }
      }, this.computedIcon);
    },

    classes() {
      const classes = { ...VSheet["a" /* default */].options.computed.classes.call(this),
        'v-alert--border': Boolean(this.border),
        'v-alert--dense': this.dense,
        'v-alert--outlined': this.outlined,
        'v-alert--prominent': this.prominent,
        'v-alert--text': this.text
      };

      if (this.border) {
        classes[`v-alert--border-${this.border}`] = true;
      }

      return classes;
    },

    computedColor() {
      return this.color || this.type;
    },

    computedIcon() {
      if (this.icon === false) return false;
      if (typeof this.icon === 'string' && this.icon) return this.icon;
      if (!['error', 'info', 'success', 'warning'].includes(this.type)) return false;
      return `$${this.type}`;
    },

    hasColoredIcon() {
      return this.hasText || Boolean(this.border) && this.coloredBorder;
    },

    hasText() {
      return this.text || this.outlined;
    },

    iconColor() {
      return this.hasColoredIcon ? this.computedColor : undefined;
    },

    isDark() {
      if (this.type && !this.coloredBorder && !this.outlined) return true;
      return themeable["a" /* default */].options.computed.isDark.call(this);
    }

  },

  created() {
    /* istanbul ignore next */
    if (this.$attrs.hasOwnProperty('outline')) {
      Object(console["a" /* breaking */])('outline', 'outlined', this);
    }
  },

  methods: {
    genWrapper() {
      const children = [this.$slots.prepend || this.__cachedIcon, this.genContent(), this.__cachedBorder, this.$slots.append, this.$scopedSlots.close ? this.$scopedSlots.close({
        toggle: this.toggle
      }) : this.__cachedDismissible];
      const data = {
        staticClass: 'v-alert__wrapper'
      };
      return this.$createElement('div', data, children);
    },

    genContent() {
      return this.$createElement('div', {
        staticClass: 'v-alert__content'
      }, this.$slots.default);
    },

    genAlert() {
      let data = {
        staticClass: 'v-alert',
        attrs: {
          role: 'alert'
        },
        on: this.listeners$,
        class: this.classes,
        style: this.styles,
        directives: [{
          name: 'show',
          value: this.isActive
        }]
      };

      if (!this.coloredBorder) {
        const setColor = this.hasText ? this.setTextColor : this.setBackgroundColor;
        data = setColor(this.computedColor, data);
      }

      return this.$createElement('div', data, [this.genWrapper()]);
    },

    /** @public */
    toggle() {
      this.isActive = !this.isActive;
    }

  },

  render(h) {
    const render = this.genAlert();
    if (!this.transition) return render;
    return h('transition', {
      props: {
        name: this.transition,
        origin: this.origin,
        mode: this.mode
      }
    }, [render]);
  }

}));

/***/ }),

/***/ 282:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(300);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("37cc6ed2", content, true)

/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(299);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("0769cd4a", content, true)

/***/ }),

/***/ 299:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".theme--light.v-calendar-events .v-event-timed{border:1px solid!important}.theme--light.v-calendar-events .v-event-more{background-color:#fff}.theme--light.v-calendar-events .v-event-more.v-outside{background-color:#f7f7f7}.theme--dark.v-calendar-events .v-event-timed{border:1px solid!important}.theme--dark.v-calendar-events .v-event-more{background-color:#303030}.theme--dark.v-calendar-events .v-event-more.v-outside{background-color:#202020}.v-calendar .v-event{line-height:20px;margin-right:-1px;border-radius:4px}.v-calendar .v-event,.v-calendar .v-event-more{position:relative;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;font-size:12px;cursor:pointer;z-index:1}.v-calendar .v-event-more{font-weight:700}.v-calendar .v-event-timed-container{position:absolute;top:0;bottom:0;left:0;right:0;margin-right:10px;pointer-events:none}.v-calendar .v-event-timed{position:absolute;white-space:nowrap;text-overflow:ellipsis;font-size:12px;cursor:pointer;border-radius:4px;pointer-events:all}.v-calendar .v-event-summary{display:inline-block;overflow:hidden;text-overflow:ellipsis;width:100%;white-space:nowrap}.v-calendar.v-calendar-events .v-calendar-weekly__head-weekday{margin-right:-1px}.v-calendar.v-calendar-events .v-calendar-weekly__day{overflow:visible;margin-right:-1px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 300:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".theme--light.v-calendar-weekly{background-color:#fff;border-top:1px solid #e0e0e0;border-left:1px solid #e0e0e0}.theme--light.v-calendar-weekly .v-calendar-weekly__head-weekday{border-right:1px solid #e0e0e0;color:#000}.theme--light.v-calendar-weekly .v-calendar-weekly__head-weekday.v-past{color:rgba(0,0,0,.38)}.theme--light.v-calendar-weekly .v-calendar-weekly__head-weekday.v-outside{background-color:#f7f7f7}.theme--light.v-calendar-weekly .v-calendar-weekly__head-weeknumber{background-color:#f1f3f4;border-right:1px solid #e0e0e0}.theme--light.v-calendar-weekly .v-calendar-weekly__day{border-right:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;color:#000}.theme--light.v-calendar-weekly .v-calendar-weekly__day.v-outside{background-color:#f7f7f7}.theme--light.v-calendar-weekly .v-calendar-weekly__weeknumber{background-color:#f1f3f4;border-right:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;color:#000}.theme--dark.v-calendar-weekly{background-color:#303030;border-top:1px solid #9e9e9e;border-left:1px solid #9e9e9e}.theme--dark.v-calendar-weekly .v-calendar-weekly__head-weekday{border-right:1px solid #9e9e9e;color:#fff}.theme--dark.v-calendar-weekly .v-calendar-weekly__head-weekday.v-past{color:hsla(0,0%,100%,.5)}.theme--dark.v-calendar-weekly .v-calendar-weekly__head-weekday.v-outside{background-color:#202020}.theme--dark.v-calendar-weekly .v-calendar-weekly__head-weeknumber{background-color:#202020;border-right:1px solid #9e9e9e}.theme--dark.v-calendar-weekly .v-calendar-weekly__day{border-right:1px solid #9e9e9e;border-bottom:1px solid #9e9e9e;color:#fff}.theme--dark.v-calendar-weekly .v-calendar-weekly__day.v-outside{background-color:#202020}.theme--dark.v-calendar-weekly .v-calendar-weekly__weeknumber{background-color:#202020;border-right:1px solid #9e9e9e;border-bottom:1px solid #9e9e9e;color:#fff}.v-calendar-weekly{width:100%;height:100%;display:flex;flex-direction:column;min-height:0}.v-calendar-weekly__head{display:flex}.v-calendar-weekly__head,.v-calendar-weekly__head-weekday{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-calendar-weekly__head-weekday{flex:1 0 20px;padding:0 4px;font-size:11px;overflow:hidden;text-align:center;text-overflow:ellipsis;text-transform:uppercase;white-space:nowrap}.v-calendar-weekly__head-weeknumber{position:relative;flex:0 0 24px}.v-calendar-weekly__week{display:flex;flex:1;height:unset;min-height:0}.v-calendar-weekly__weeknumber{display:flex;flex:0 0 24px;height:unset;min-height:0;padding-top:14.5px;text-align:center}.v-calendar-weekly__weeknumber>small{width:100%!important}.v-calendar-weekly__day{flex:1;width:0;overflow:hidden;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;position:relative;padding:0;min-width:0}.v-calendar-weekly__day.v-present .v-calendar-weekly__day-month{color:currentColor}.v-calendar-weekly__day-label{text-decoration:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;box-shadow:none;text-align:center;margin:4px 0 0}.v-calendar-weekly__day-label .v-btn{font-size:12px;text-transform:none}.v-calendar-weekly__day-month{position:absolute;text-decoration:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;box-shadow:none;top:0;left:36px;height:32px;line-height:32px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 301:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(302);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("1ffbeeca", content, true)

/***/ }),

/***/ 302:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".theme--light.v-calendar-daily{background-color:#fff;border-left:1px solid #e0e0e0;border-top:1px solid #e0e0e0}.theme--light.v-calendar-daily .v-calendar-daily__intervals-head{border-right:1px solid #e0e0e0}.theme--light.v-calendar-daily .v-calendar-daily__intervals-head:after{background:#e0e0e0;background:linear-gradient(90deg,transparent,#e0e0e0)}.theme--light.v-calendar-daily .v-calendar-daily_head-day{border-right:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;color:#000}.theme--light.v-calendar-daily .v-calendar-daily_head-day.v-past .v-calendar-daily_head-day-label,.theme--light.v-calendar-daily .v-calendar-daily_head-day.v-past .v-calendar-daily_head-weekday{color:rgba(0,0,0,.38)}.theme--light.v-calendar-daily .v-calendar-daily__intervals-body{border-right:1px solid #e0e0e0}.theme--light.v-calendar-daily .v-calendar-daily__intervals-body .v-calendar-daily__interval-text{color:#424242}.theme--light.v-calendar-daily .v-calendar-daily__day{border-right:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0}.theme--light.v-calendar-daily .v-calendar-daily__day-interval{border-top:1px solid #e0e0e0}.theme--light.v-calendar-daily .v-calendar-daily__day-interval:first-child{border-top:none!important}.theme--light.v-calendar-daily .v-calendar-daily__interval:after{border-top:1px solid #e0e0e0}.theme--dark.v-calendar-daily{background-color:#303030;border-left:1px solid #9e9e9e;border-top:1px solid #9e9e9e}.theme--dark.v-calendar-daily .v-calendar-daily__intervals-head{border-right:1px solid #9e9e9e}.theme--dark.v-calendar-daily .v-calendar-daily__intervals-head:after{background:#9e9e9e;background:linear-gradient(90deg,transparent,#9e9e9e)}.theme--dark.v-calendar-daily .v-calendar-daily_head-day{border-right:1px solid #9e9e9e;border-bottom:1px solid #9e9e9e;color:#fff}.theme--dark.v-calendar-daily .v-calendar-daily_head-day.v-past .v-calendar-daily_head-day-label,.theme--dark.v-calendar-daily .v-calendar-daily_head-day.v-past .v-calendar-daily_head-weekday{color:hsla(0,0%,100%,.5)}.theme--dark.v-calendar-daily .v-calendar-daily__intervals-body{border-right:1px solid #9e9e9e}.theme--dark.v-calendar-daily .v-calendar-daily__intervals-body .v-calendar-daily__interval-text{color:#eee}.theme--dark.v-calendar-daily .v-calendar-daily__day{border-right:1px solid #9e9e9e;border-bottom:1px solid #9e9e9e}.theme--dark.v-calendar-daily .v-calendar-daily__day-interval{border-top:1px solid #9e9e9e}.theme--dark.v-calendar-daily .v-calendar-daily__day-interval:first-child{border-top:none!important}.theme--dark.v-calendar-daily .v-calendar-daily__interval:after{border-top:1px solid #9e9e9e}.v-calendar-daily{display:flex;flex-direction:column;overflow:hidden;height:100%}.v-calendar-daily__head{flex:none;display:flex}.v-calendar-daily__intervals-head{flex:none;position:relative}.v-calendar-daily__intervals-head:after{position:absolute;bottom:0;height:1px;left:0;right:0;content:\"\"}.v-calendar-daily_head-day{flex:1 1 auto;width:0;position:relative}.v-calendar-daily_head-weekday{padding:3px 0 0;font-size:11px;text-transform:uppercase}.v-calendar-daily_head-day-label,.v-calendar-daily_head-weekday{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;text-align:center}.v-calendar-daily_head-day-label{padding:0 0 3px;cursor:pointer}.v-calendar-daily__body{flex:1 1 60%;overflow:hidden;display:flex;position:relative;flex-direction:column}.v-calendar-daily__scroll-area{overflow-y:scroll;flex:1 1 auto;display:flex;align-items:flex-start}.v-calendar-daily__pane{width:100%;overflow-y:hidden;flex:none;display:flex;align-items:flex-start}.v-calendar-daily__day-container{display:flex;flex:1;width:100%;height:100%}.v-calendar-daily__intervals-body{flex:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.v-calendar-daily__interval{text-align:right;padding-right:8px;border-bottom:none;position:relative}.v-calendar-daily__interval:after{width:8px;position:absolute;height:1px;display:block;content:\"\";right:0;bottom:-1px}.v-calendar-daily__interval-text{display:block;position:relative;top:-6px;font-size:10px;padding-right:4px}.v-calendar-daily__day{flex:1;width:0;position:relative}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(304);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
__webpack_require__(5).default("2b825c72", content, true)

/***/ }),

/***/ 304:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".theme--light.v-calendar-category .v-calendar-category__column,.theme--light.v-calendar-category .v-calendar-category__column-header{border-right:1px solid #e0e0e0}.theme--light.v-calendar-category .v-calendar-daily__head,.theme--light.v-calendar-category .v-calendar-daily__intervals-body,.theme--light.v-calendar-category .v-calendar-daily__intervals-head{background:#fff}.theme--dark.v-calendar-category .v-calendar-category__column,.theme--dark.v-calendar-category .v-calendar-category__column-header{border-right:1px solid #9e9e9e}.theme--dark.v-calendar-category .v-calendar-daily__head,.theme--dark.v-calendar-category .v-calendar-daily__intervals-body,.theme--dark.v-calendar-category .v-calendar-daily__intervals-head{background:#303030}.v-calendar-category{overflow:auto;position:relative}.v-calendar-category .v-calendar-category__category{text-align:center}.v-calendar-category .v-calendar-daily__day-container{width:-webkit-min-content;width:-moz-min-content;width:min-content}.v-calendar-category .v-calendar-daily__day-container .v-calendar-category__columns{position:absolute;height:100%;width:100%;top:0}.v-calendar-category .v-calendar-daily__day-body{display:flex;flex:1;width:100%;height:100%}.v-calendar-category .v-calendar-daily__head{flex-direction:row;width:-webkit-min-content;width:-moz-min-content;width:min-content;min-width:100%;position:-webkit-sticky;position:sticky;top:0;z-index:2}.v-calendar-category .v-calendar-daily_head-day{width:auto;position:unset}.v-calendar-category .v-calendar-daily__intervals-head{position:-webkit-sticky;position:sticky;left:0;top:0;z-index:2}.v-calendar-category .v-calendar-daily_head-weekday{position:-webkit-sticky;position:sticky;left:50%;width:50px}.v-calendar-category .v-calendar-daily_head-day-label{width:56px;position:-webkit-sticky;position:sticky;left:50%}.v-calendar-category .v-calendar-daily__day{min-width:200px}.v-calendar-category .v-calendar-daily__intervals-body{position:-webkit-sticky;position:sticky;left:0;z-index:1}.v-calendar-category .v-calendar-daily__interval:last-of-type:after{display:none}.v-calendar-category .v-calendar-daily__body{overflow:visible}.v-calendar-category .v-calendar-daily__body .v-calendar-daily__scroll-area{overflow-y:visible;flex:none}.v-calendar-category .v-calendar-daily__pane{overflow-y:visible}.v-calendar-category .v-calendar-category__columns{display:flex;width:-webkit-min-content;width:-moz-min-content;width:min-content;min-width:100%}.v-calendar-category .v-calendar-category__columns .v-calendar-category__column,.v-calendar-category .v-calendar-category__columns .v-calendar-category__column-header{flex:1 1 auto;width:0;position:relative}.v-calendar-category .v-calendar-category__columns .v-calendar-category__column-header{min-width:200px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/inicio.vue?vue&type=template&id=8762329c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-container',[_c('v-alert',{staticStyle:{"text-align":"center"},attrs:{"border":"top","color":"indigo","dark":""}},[_c('b',[_vm._v("Bienvenido al registro de Tratamientos y Pacientes.")])]),_vm._v(" "),_c('v-row',{staticClass:"fill-height"},[_c('v-col',[_c('v-sheet',{attrs:{"height":"64"}},[_c('v-toolbar',{attrs:{"flat":""}},[_c('v-btn',{staticClass:"mr-4",attrs:{"outlined":"","color":"grey darken-2"},on:{"click":_vm.setToday}},[_vm._v("Hoy")]),_vm._v(" "),_c('v-btn',{attrs:{"fab":"","text":"","small":"","color":"grey darken-2"},on:{"click":_vm.prev}},[_c('v-icon',{attrs:{"small":""}},[_vm._v("mdi-chevron-left")])],1),_vm._v(" "),_c('v-btn',{attrs:{"fab":"","text":"","small":"","color":"grey darken-2"},on:{"click":_vm.next}},[_c('v-icon',{attrs:{"small":""}},[_vm._v("\n            mdi-chevron-right\n          ")])],1),_vm._v(" "),(_vm.$refs.calendar)?_c('v-toolbar-title',[_vm._v("\n          "+_vm._s(_vm.$refs.calendar.title)+"\n        ")]):_vm._e(),_vm._v(" "),_c('v-spacer'),_vm._v(" "),_c('v-menu',{attrs:{"bottom":"","right":""},scopedSlots:_vm._u([{key:"activator",fn:function(ref){
var on = ref.on;
var attrs = ref.attrs;
return [_c('v-btn',_vm._g(_vm._b({attrs:{"outlined":"","color":"grey darken-2"}},'v-btn',attrs,false),on),[_c('span',[_vm._v(_vm._s(_vm.typeToLabel[_vm.type]))]),_vm._v(" "),_c('v-icon',{attrs:{"right":""}},[_vm._v("\n                mdi-menu-down\n              ")])],1)]}}])},[_vm._v(" "),_c('v-list',[_c('v-list-item',{on:{"click":function($event){_vm.type = 'day'}}},[_c('v-list-item-title',[_vm._v("Día")])],1),_vm._v(" "),_c('v-list-item',{on:{"click":function($event){_vm.type = 'week'}}},[_c('v-list-item-title',[_vm._v("Semana")])],1),_vm._v(" "),_c('v-list-item',{on:{"click":function($event){_vm.type = 'month'}}},[_c('v-list-item-title',[_vm._v("Mes")])],1),_vm._v(" "),_c('v-list-item',{on:{"click":function($event){_vm.type = '4day'}}},[_c('v-list-item-title',[_vm._v("4 días")])],1)],1)],1)],1)],1),_vm._v(" "),_c('v-sheet',{attrs:{"height":"600"}},[_c('v-calendar',{ref:"calendar",attrs:{"color":"primary","events":_vm.events,"event-color":_vm.getEventColor,"type":_vm.type},on:{"click:event":_vm.showEvent,"click:more":_vm.viewDay,"click:date":_vm.viewDay,"change":_vm.updateRange},model:{value:(_vm.focus),callback:function ($$v) {_vm.focus=$$v},expression:"focus"}}),_vm._v(" "),_c('v-menu',{attrs:{"close-on-content-click":false,"activator":_vm.selectedElement,"offset-x":""},model:{value:(_vm.selectedOpen),callback:function ($$v) {_vm.selectedOpen=$$v},expression:"selectedOpen"}},[_c('v-card',{attrs:{"color":"grey lighten-4","min-width":"350px","flat":""}},[_c('v-toolbar',{attrs:{"color":_vm.selectedEvent.color,"dark":""}},[_c('v-toolbar-title',{domProps:{"innerHTML":_vm._s(_vm.selectedEvent.name)}}),_vm._v(" "),_c('v-spacer')],1),_vm._v(" "),_c('v-card-text',[_vm._v("\n            "+_vm._s(_vm.selectedEvent.start)+" - "+_vm._s(_vm.selectedEvent.end)+"\n          ")]),_vm._v(" "),_c('v-card-actions',[_c('v-btn',{attrs:{"text":"","color":"secondary"},on:{"click":function($event){_vm.selectedOpen = false}}},[_vm._v("\n              Cerrar\n            ")])],1)],1)],1)],1)],1)],1)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/inicio.vue?vue&type=template&id=8762329c&

// EXTERNAL MODULE: external "vuex"
var external_vuex_ = __webpack_require__(31);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/inicio.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var iniciovue_type_script_lang_js_ = ({
  created() {
    this.$nuxt.$emit('ActualizaLayout', true);
  },

  computed: { ...Object(external_vuex_["mapGetters"])(['isAuthenticated', 'getSession'])
  },

  mounted() {
    this.$refs.calendar.checkChange();
  },

  data() {
    return {
      focus: '',
      type: 'month',
      typeToLabel: {
        month: 'Mes',
        week: 'Semana',
        day: 'Día',
        '4day': '4 Días'
      },
      selectedEvent: {},
      selectedElement: null,
      selectedOpen: false,
      events: [],
      colors: ['blue', 'indigo', 'deep-purple', 'cyan', 'green', 'orange', 'grey darken-1'],
      names: ['Meeting', 'Holiday', 'PTO', 'Travel', 'Event', 'Birthday', 'Conference', 'Party']
    };
  },

  methods: {
    viewDay({
      date
    }) {
      this.focus = date;
      this.type = 'day';
    },

    getEventColor(event) {
      return event.color;
    },

    setToday() {
      this.focus = '';
    },

    prev() {
      this.$refs.calendar.prev();
    },

    next() {
      this.$refs.calendar.next();
    },

    showEvent({
      nativeEvent,
      event
    }) {
      const open = () => {
        this.selectedEvent = event;
        this.selectedElement = nativeEvent.target;
        requestAnimationFrame(() => requestAnimationFrame(() => this.selectedOpen = true));
      };

      if (this.selectedOpen) {
        this.selectedOpen = false;
        requestAnimationFrame(() => requestAnimationFrame(() => open()));
      } else {
        open();
      }

      nativeEvent.stopPropagation();
    },

    async updateRange({
      start,
      end
    }) {
      let fecha_inicio = '';
      let fecha_fin = '';
      fecha_inicio = await this.$utils.formatoFechaEspañol(start.date);
      fecha_fin = await this.$utils.formatoFechaEspañol(end.date);
      await this.$service.get('DENTAL', `reservas/calendario?res_fecha_inicio=${fecha_inicio}&res_fecha_final=${fecha_fin}`).then(async response => {
        if (response) {
          this.events = response;
          this.$forceUpdate();
        }
      }).catch(error => {
        this.$toast.info(error.error_mensaje);
        this.events = [];
      }); // const events = []
      // const min = new Date(`${start.date}T00:00:00`)
      // const max = new Date(`${end.date}T23:59:59`)
      // const days = (max.getTime() - min.getTime()) / 86400000
      // const eventCount = this.rnd(days, days + 1)
      // for (let i = 0; i < eventCount; i++) {
      //   const allDay = this.rnd(0, 3) === 0
      //   const firstTimestamp = this.rnd(min.getTime(), max.getTime())
      //   const first = new Date(firstTimestamp - (firstTimestamp % 900000))
      //   const secondTimestamp = this.rnd(2, allDay ? 288 : 8) * 900000
      //   const second = new Date(first.getTime() + secondTimestamp)
      //   events.push({
      //     name: this.names[this.rnd(0, this.names.length - 1)],
      //     start: first,
      //     end: second,
      //     color: this.colors[this.rnd(0, this.colors.length - 1)],
      //     timed: !allDay,
      //   })
      //   console.log(JSON.stringify(events))
      // }
      // this.events = events
      // console.log(this.events)
      // console.log(JSON.stringify(this.events))
    },

    rnd(a, b) {
      return Math.floor((b - a + 1) * Math.random()) + a;
    }

  }
});
// CONCATENATED MODULE: ./pages/inicio.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_iniciovue_type_script_lang_js_ = (iniciovue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(8);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(9);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VAlert/VAlert.js + 1 modules
var VAlert = __webpack_require__(271);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/VBtn.js + 2 modules
var VBtn = __webpack_require__(46);

// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VCalendar/mixins/calendar-with-events.sass
var calendar_with_events = __webpack_require__(298);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/directives/ripple/index.js
var ripple = __webpack_require__(22);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/mixins.js
var mixins = __webpack_require__(2);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/colorable/index.js
var colorable = __webpack_require__(7);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/localable/index.js
var localable = __webpack_require__(25);

// EXTERNAL MODULE: external "vue"
var external_vue_ = __webpack_require__(1);
var external_vue_default = /*#__PURE__*/__webpack_require__.n(external_vue_);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/mixins/mouse.js

/* harmony default export */ var mouse = (external_vue_default.a.extend({
  name: 'mouse',
  methods: {
    getDefaultMouseEventHandlers(suffix, getEvent) {
      return this.getMouseEventHandlers({
        ['click' + suffix]: {
          event: 'click'
        },
        ['contextmenu' + suffix]: {
          event: 'contextmenu',
          prevent: true,
          result: false
        },
        ['mousedown' + suffix]: {
          event: 'mousedown'
        },
        ['mousemove' + suffix]: {
          event: 'mousemove'
        },
        ['mouseup' + suffix]: {
          event: 'mouseup'
        },
        ['mouseenter' + suffix]: {
          event: 'mouseenter'
        },
        ['mouseleave' + suffix]: {
          event: 'mouseleave'
        },
        ['touchstart' + suffix]: {
          event: 'touchstart'
        },
        ['touchmove' + suffix]: {
          event: 'touchmove'
        },
        ['touchend' + suffix]: {
          event: 'touchend'
        }
      }, getEvent);
    },

    getMouseEventHandlers(events, getEvent) {
      const on = {};

      for (const event in events) {
        const eventOptions = events[event];
        if (!this.$listeners[event]) continue; // TODO somehow pull in modifiers

        const prefix = eventOptions.passive ? '&' : (eventOptions.once ? '~' : '') + (eventOptions.capture ? '!' : '');
        const key = prefix + eventOptions.event;

        const handler = e => {
          const mouseEvent = e;

          if (eventOptions.button === undefined || mouseEvent.buttons > 0 && mouseEvent.button === eventOptions.button) {
            if (eventOptions.prevent) {
              e.preventDefault();
            }

            if (eventOptions.stop) {
              e.stopPropagation();
            } // Due to TouchEvent target always returns the element that is first placed
            // Even if touch point has since moved outside the interactive area of that element
            // Ref: https://developer.mozilla.org/en-US/docs/Web/API/Touch/target
            // This block of code aims to make sure touchEvent is always dispatched from the element that is being pointed at


            if (e && 'touches' in e) {
              var _e$currentTarget, _e$target;

              const classSeparator = ' ';
              const eventTargetClasses = (_e$currentTarget = e.currentTarget) == null ? void 0 : _e$currentTarget.className.split(classSeparator);
              const currentTargets = document.elementsFromPoint(e.changedTouches[0].clientX, e.changedTouches[0].clientY); // Get "the same kind" current hovering target by checking
              // If element has the same class of initial touch start element (which has touch event listener registered)

              const currentTarget = currentTargets.find(t => t.className.split(classSeparator).some(c => eventTargetClasses.includes(c)));

              if (currentTarget && !((_e$target = e.target) != null && _e$target.isSameNode(currentTarget))) {
                currentTarget.dispatchEvent(new TouchEvent(e.type, {
                  changedTouches: e.changedTouches,
                  targetTouches: e.targetTouches,
                  touches: e.touches
                }));
                return;
              }
            }

            this.$emit(event, getEvent(e), e);
          }

          return eventOptions.result;
        };

        if (key in on) {
          /* istanbul ignore next */
          if (Array.isArray(on[key])) {
            on[key].push(handler);
          } else {
            on[key] = [on[key], handler];
          }
        } else {
          on[key] = handler;
        }
      }

      return on;
    }

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/mixins/themeable/index.js
var themeable = __webpack_require__(6);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCalendar/util/timestamp.js
var util_timestamp = __webpack_require__(102);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/mixins/times.js


/* harmony default export */ var mixins_times = (external_vue_default.a.extend({
  name: 'times',
  props: {
    now: {
      type: String,
      validator: util_timestamp["F" /* validateTimestamp */]
    }
  },
  data: () => ({
    times: {
      now: Object(util_timestamp["v" /* parseTimestamp */])('0000-00-00 00:00', true),
      today: Object(util_timestamp["v" /* parseTimestamp */])('0000-00-00', true)
    }
  }),
  computed: {
    parsedNow() {
      return this.now ? Object(util_timestamp["v" /* parseTimestamp */])(this.now, true) : null;
    }

  },
  watch: {
    parsedNow: 'updateTimes'
  },

  created() {
    this.updateTimes();
    this.setPresent();
  },

  methods: {
    setPresent() {
      this.times.now.present = this.times.today.present = true;
      this.times.now.past = this.times.today.past = false;
      this.times.now.future = this.times.today.future = false;
    },

    updateTimes() {
      const now = this.parsedNow || this.getNow();
      this.updateDay(now, this.times.now);
      this.updateTime(now, this.times.now);
      this.updateDay(now, this.times.today);
    },

    getNow() {
      return Object(util_timestamp["t" /* parseDate */])(new Date());
    },

    updateDay(now, target) {
      if (now.date !== target.date) {
        target.year = now.year;
        target.month = now.month;
        target.day = now.day;
        target.weekday = now.weekday;
        target.date = now.date;
      }
    },

    updateTime(now, target) {
      if (now.time !== target.time) {
        target.hour = now.hour;
        target.minute = now.minute;
        target.time = now.time;
      }
    }

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/directives/resize/index.js
var resize = __webpack_require__(35);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/modes/common.js

const MILLIS_IN_DAY = 86400000;
function getVisuals(events, minStart = 0) {
  const visuals = events.map(event => ({
    event,
    columnCount: 0,
    column: 0,
    left: 0,
    width: 100
  }));
  visuals.sort((a, b) => {
    return Math.max(minStart, a.event.startTimestampIdentifier) - Math.max(minStart, b.event.startTimestampIdentifier) || b.event.endTimestampIdentifier - a.event.endTimestampIdentifier;
  });
  return visuals;
}
function hasOverlap(s0, e0, s1, e1, exclude = true) {
  return exclude ? !(s0 >= e1 || e0 <= s1) : !(s0 > e1 || e0 < s1);
}
function setColumnCount(groups) {
  groups.forEach(group => {
    group.visuals.forEach(groupVisual => {
      groupVisual.columnCount = groups.length;
    });
  });
}
function getRange(event) {
  return [event.startTimestampIdentifier, event.endTimestampIdentifier];
}
function getDayRange(event) {
  return [event.startIdentifier, event.endIdentifier];
}
function getNormalizedRange(event, dayStart) {
  return [Math.max(dayStart, event.startTimestampIdentifier), Math.min(dayStart + MILLIS_IN_DAY, event.endTimestampIdentifier)];
}
function getOpenGroup(groups, start, end, timed) {
  for (let i = 0; i < groups.length; i++) {
    const group = groups[i];
    let intersected = false;

    if (hasOverlap(start, end, group.start, group.end, timed)) {
      for (let k = 0; k < group.visuals.length; k++) {
        const groupVisual = group.visuals[k];
        const [groupStart, groupEnd] = timed ? getRange(groupVisual.event) : getDayRange(groupVisual.event);

        if (hasOverlap(start, end, groupStart, groupEnd, timed)) {
          intersected = true;
          break;
        }
      }
    }

    if (!intersected) {
      return i;
    }
  }

  return -1;
}
function getOverlapGroupHandler(firstWeekday) {
  const handler = {
    groups: [],
    min: -1,
    max: -1,
    reset: () => {
      handler.groups = [];
      handler.min = handler.max = -1;
    },
    getVisuals: (day, dayEvents, timed, reset = false) => {
      if (day.weekday === firstWeekday || reset) {
        handler.reset();
      }

      const dayStart = Object(util_timestamp["p" /* getTimestampIdentifier */])(day);
      const visuals = getVisuals(dayEvents, dayStart);
      visuals.forEach(visual => {
        const [start, end] = timed ? getRange(visual.event) : getDayRange(visual.event);

        if (handler.groups.length > 0 && !hasOverlap(start, end, handler.min, handler.max, timed)) {
          setColumnCount(handler.groups);
          handler.reset();
        }

        let targetGroup = getOpenGroup(handler.groups, start, end, timed);

        if (targetGroup === -1) {
          targetGroup = handler.groups.length;
          handler.groups.push({
            start,
            end,
            visuals: []
          });
        }

        const target = handler.groups[targetGroup];
        target.visuals.push(visual);
        target.start = Math.min(target.start, start);
        target.end = Math.max(target.end, end);
        visual.column = targetGroup;

        if (handler.min === -1) {
          handler.min = start;
          handler.max = end;
        } else {
          handler.min = Math.min(handler.min, start);
          handler.max = Math.max(handler.max, end);
        }
      });
      setColumnCount(handler.groups);

      if (timed) {
        handler.reset();
      }

      return visuals;
    }
  };
  return handler;
}
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/modes/stack.js


const FULL_WIDTH = 100;
const DEFAULT_OFFSET = 5;
const WIDTH_MULTIPLIER = 1.7;
/**
 * Variation of column mode where events can be stacked. The priority of this
 * mode is to stack events together taking up the least amount of space while
 * trying to ensure the content of the event is always visible as well as its
 * start and end. A sibling column has intersecting event content and must be
 * placed beside each other. Non-sibling columns are offset by 5% from the
 * previous column. The width is scaled by 1.7 so the events overlap and
 * whitespace is reduced. If there is a hole in columns the event width is
 * scaled up so it intersects with the next column. The columns have equal
 * width in the space they are given. If the event doesn't have any to the
 * right of it that intersect with it's content it's right side is extended
 * to the right side.
 */

const stack = (events, firstWeekday, overlapThreshold) => {
  const handler = getOverlapGroupHandler(firstWeekday); // eslint-disable-next-line max-statements

  return (day, dayEvents, timed, reset) => {
    if (!timed) {
      return handler.getVisuals(day, dayEvents, timed, reset);
    }

    const dayStart = Object(util_timestamp["p" /* getTimestampIdentifier */])(day);
    const visuals = getVisuals(dayEvents, dayStart);
    const groups = getGroups(visuals, dayStart);

    for (const group of groups) {
      const nodes = [];

      for (const visual of group.visuals) {
        const child = getNode(visual, dayStart);
        const index = getNextIndex(child, nodes);

        if (index === false) {
          const parent = getParent(child, nodes);

          if (parent) {
            child.parent = parent;
            child.sibling = hasOverlap(child.start, child.end, parent.start, addTime(parent.start, overlapThreshold));
            child.index = parent.index + 1;
            parent.children.push(child);
          }
        } else {
          const [parent] = getOverlappingRange(child, nodes, index - 1, index - 1);
          const children = getOverlappingRange(child, nodes, index + 1, index + nodes.length, true);
          child.children = children;
          child.index = index;

          if (parent) {
            child.parent = parent;
            child.sibling = hasOverlap(child.start, child.end, parent.start, addTime(parent.start, overlapThreshold));
            parent.children.push(child);
          }

          for (const grand of children) {
            if (grand.parent === parent) {
              grand.parent = child;
            }

            const grandNext = grand.index - child.index <= 1;

            if (grandNext && child.sibling && hasOverlap(child.start, addTime(child.start, overlapThreshold), grand.start, grand.end)) {
              grand.sibling = true;
            }
          }
        }

        nodes.push(child);
      }

      calculateBounds(nodes, overlapThreshold);
    }

    visuals.sort((a, b) => a.left - b.left || a.event.startTimestampIdentifier - b.event.startTimestampIdentifier);
    return visuals;
  };
};

function calculateBounds(nodes, overlapThreshold) {
  for (const node of nodes) {
    const {
      visual,
      parent
    } = node;
    const columns = getMaxChildIndex(node) + 1;
    const spaceLeft = parent ? parent.visual.left : 0;
    const spaceWidth = FULL_WIDTH - spaceLeft;
    const offset = Math.min(DEFAULT_OFFSET, FULL_WIDTH / columns);
    const columnWidthMultiplier = getColumnWidthMultiplier(node, nodes);
    const columnOffset = spaceWidth / (columns - node.index + 1);
    const columnWidth = spaceWidth / (columns - node.index + (node.sibling ? 1 : 0)) * columnWidthMultiplier;

    if (parent) {
      visual.left = node.sibling ? spaceLeft + columnOffset : spaceLeft + offset;
    }

    visual.width = hasFullWidth(node, nodes, overlapThreshold) ? FULL_WIDTH - visual.left : Math.min(FULL_WIDTH - visual.left, columnWidth * WIDTH_MULTIPLIER);
  }
}

function getColumnWidthMultiplier(node, nodes) {
  if (!node.children.length) {
    return 1;
  }

  const maxColumn = node.index + nodes.length;
  const minColumn = node.children.reduce((min, c) => Math.min(min, c.index), maxColumn);
  return minColumn - node.index;
}

function getOverlappingIndices(node, nodes) {
  const indices = [];

  for (const other of nodes) {
    if (hasOverlap(node.start, node.end, other.start, other.end)) {
      indices.push(other.index);
    }
  }

  return indices;
}

function getNextIndex(node, nodes) {
  const indices = getOverlappingIndices(node, nodes);
  indices.sort();

  for (let i = 0; i < indices.length; i++) {
    if (i < indices[i]) {
      return i;
    }
  }

  return false;
}

function getOverlappingRange(node, nodes, indexMin, indexMax, returnFirstColumn = false) {
  const overlapping = [];

  for (const other of nodes) {
    if (other.index >= indexMin && other.index <= indexMax && hasOverlap(node.start, node.end, other.start, other.end)) {
      overlapping.push(other);
    }
  }

  if (returnFirstColumn && overlapping.length > 0) {
    const first = overlapping.reduce((min, n) => Math.min(min, n.index), overlapping[0].index);
    return overlapping.filter(n => n.index === first);
  }

  return overlapping;
}

function getParent(node, nodes) {
  let parent = null;

  for (const other of nodes) {
    if (hasOverlap(node.start, node.end, other.start, other.end) && (parent === null || other.index > parent.index)) {
      parent = other;
    }
  }

  return parent;
}

function hasFullWidth(node, nodes, overlapThreshold) {
  for (const other of nodes) {
    if (other !== node && other.index > node.index && hasOverlap(node.start, addTime(node.start, overlapThreshold), other.start, other.end)) {
      return false;
    }
  }

  return true;
}

function getGroups(visuals, dayStart) {
  const groups = [];

  for (const visual of visuals) {
    const [start, end] = getNormalizedRange(visual.event, dayStart);
    let added = false;

    for (const group of groups) {
      if (hasOverlap(start, end, group.start, group.end)) {
        group.visuals.push(visual);
        group.end = Math.max(group.end, end);
        added = true;
        break;
      }
    }

    if (!added) {
      groups.push({
        start,
        end,
        visuals: [visual]
      });
    }
  }

  return groups;
}

function getNode(visual, dayStart) {
  const [start, end] = getNormalizedRange(visual.event, dayStart);
  return {
    parent: null,
    sibling: true,
    index: 0,
    visual,
    start,
    end,
    children: []
  };
}

function getMaxChildIndex(node) {
  let max = node.index;

  for (const child of node.children) {
    const childMax = getMaxChildIndex(child);

    if (childMax > max) {
      max = childMax;
    }
  }

  return max;
}

function addTime(identifier, minutes) {
  const removeMinutes = identifier % 100;
  const totalMinutes = removeMinutes + minutes;
  const addHours = Math.floor(totalMinutes / 60);
  const addMinutes = totalMinutes % 60;
  return identifier - removeMinutes + addHours * 100 + addMinutes;
}
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/modes/column.js

const column_FULL_WIDTH = 100;
const column = (events, firstWeekday, overlapThreshold) => {
  const handler = getOverlapGroupHandler(firstWeekday);
  return (day, dayEvents, timed, reset) => {
    const visuals = handler.getVisuals(day, dayEvents, timed, reset);

    if (timed) {
      visuals.forEach(visual => {
        visual.left = visual.column * column_FULL_WIDTH / visual.columnCount;
        visual.width = column_FULL_WIDTH / visual.columnCount;
      });
    }

    return visuals;
  };
};
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/modes/index.js


const CalendarEventOverlapModes = {
  stack: stack,
  column: column
};
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/util/props.js


/* harmony default export */ var props = ({
  base: {
    start: {
      type: [String, Number, Date],
      validate: util_timestamp["F" /* validateTimestamp */],
      default: () => Object(util_timestamp["t" /* parseDate */])(new Date()).date
    },
    end: {
      type: [String, Number, Date],
      validate: util_timestamp["F" /* validateTimestamp */]
    },
    weekdays: {
      type: [Array, String],
      default: () => [0, 1, 2, 3, 4, 5, 6],
      validate: validateWeekdays
    },
    hideHeader: {
      type: Boolean
    },
    shortWeekdays: {
      type: Boolean,
      default: true
    },
    weekdayFormat: {
      type: Function,
      default: null
    },
    dayFormat: {
      type: Function,
      default: null
    }
  },
  intervals: {
    maxDays: {
      type: Number,
      default: 7
    },
    shortIntervals: {
      type: Boolean,
      default: true
    },
    intervalHeight: {
      type: [Number, String],
      default: 48,
      validate: validateNumber
    },
    intervalWidth: {
      type: [Number, String],
      default: 60,
      validate: validateNumber
    },
    intervalMinutes: {
      type: [Number, String],
      default: 60,
      validate: validateNumber
    },
    firstInterval: {
      type: [Number, String],
      default: 0,
      validate: validateNumber
    },
    firstTime: {
      type: [Number, String, Object],
      validate: util_timestamp["E" /* validateTime */]
    },
    intervalCount: {
      type: [Number, String],
      default: 24,
      validate: validateNumber
    },
    intervalFormat: {
      type: Function,
      default: null
    },
    intervalStyle: {
      type: Function,
      default: null
    },
    showIntervalLabel: {
      type: Function,
      default: null
    }
  },
  weeks: {
    localeFirstDayOfYear: {
      type: [String, Number],
      default: 0
    },
    minWeeks: {
      validate: validateNumber,
      default: 1
    },
    shortMonths: {
      type: Boolean,
      default: true
    },
    showMonthOnFirst: {
      type: Boolean,
      default: true
    },
    showWeek: Boolean,
    monthFormat: {
      type: Function,
      default: null
    }
  },
  calendar: {
    type: {
      type: String,
      default: 'month'
    },
    value: {
      type: [String, Number, Date],
      validate: util_timestamp["F" /* validateTimestamp */]
    }
  },
  category: {
    categories: {
      type: [Array, String],
      default: ''
    },
    categoryText: {
      type: [String, Function]
    },
    categoryHideDynamic: {
      type: Boolean
    },
    categoryShowAll: {
      type: Boolean
    },
    categoryForInvalid: {
      type: String,
      default: ''
    },
    categoryDays: {
      type: [Number, String],
      default: 1,
      validate: x => isFinite(parseInt(x)) && parseInt(x) > 0
    }
  },
  events: {
    events: {
      type: Array,
      default: () => []
    },
    eventStart: {
      type: String,
      default: 'start'
    },
    eventEnd: {
      type: String,
      default: 'end'
    },
    eventTimed: {
      type: [String, Function],
      default: 'timed'
    },
    eventCategory: {
      type: [String, Function],
      default: 'category'
    },
    eventHeight: {
      type: Number,
      default: 20
    },
    eventColor: {
      type: [String, Function],
      default: 'primary'
    },
    eventTextColor: {
      type: [String, Function],
      default: 'white'
    },
    eventName: {
      type: [String, Function],
      default: 'name'
    },
    eventOverlapThreshold: {
      type: [String, Number],
      default: 60
    },
    eventOverlapMode: {
      type: [String, Function],
      default: 'stack',
      validate: mode => mode in CalendarEventOverlapModes || typeof mode === 'function'
    },
    eventMore: {
      type: Boolean,
      default: true
    },
    eventMoreText: {
      type: String,
      default: '$vuetify.calendar.moreEvents'
    },
    eventRipple: {
      type: [Boolean, Object],
      default: null
    },
    eventMarginBottom: {
      type: Number,
      default: 1
    }
  }
});
function validateNumber(input) {
  return isFinite(parseInt(input));
}
function validateWeekdays(input) {
  if (typeof input === 'string') {
    input = input.split(',');
  }

  if (Array.isArray(input)) {
    const ints = input.map(x => parseInt(x));

    if (ints.length > util_timestamp["b" /* DAYS_IN_WEEK */] || ints.length === 0) {
      return false;
    }

    const visited = {};
    let wrapped = false;

    for (let i = 0; i < ints.length; i++) {
      const x = ints[i];

      if (!isFinite(x) || x < 0 || x >= util_timestamp["b" /* DAYS_IN_WEEK */]) {
        return false;
      }

      if (i > 0) {
        const d = x - ints[i - 1];

        if (d < 0) {
          if (wrapped) {
            return false;
          }

          wrapped = true;
        } else if (d === 0) {
          return false;
        }
      }

      if (visited[x]) {
        return false;
      }

      visited[x] = true;
    }

    return true;
  }

  return false;
}
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/mixins/calendar-base.js
// Mixins





 // Directives

 // Util



/* harmony default export */ var calendar_base = (Object(mixins["a" /* default */])(colorable["a" /* default */], localable["a" /* default */], mouse, themeable["a" /* default */], mixins_times
/* @vue/component */
).extend({
  name: 'calendar-base',
  directives: {
    Resize: resize["a" /* default */]
  },
  props: props.base,
  computed: {
    parsedWeekdays() {
      return Array.isArray(this.weekdays) ? this.weekdays : (this.weekdays || '').split(',').map(x => parseInt(x, 10));
    },

    weekdaySkips() {
      return Object(util_timestamp["q" /* getWeekdaySkips */])(this.parsedWeekdays);
    },

    weekdaySkipsReverse() {
      const reversed = this.weekdaySkips.slice();
      reversed.reverse();
      return reversed;
    },

    parsedStart() {
      return Object(util_timestamp["v" /* parseTimestamp */])(this.start, true);
    },

    parsedEnd() {
      const start = this.parsedStart;
      const end = this.end ? Object(util_timestamp["v" /* parseTimestamp */])(this.end) || start : start;
      return Object(util_timestamp["p" /* getTimestampIdentifier */])(end) < Object(util_timestamp["p" /* getTimestampIdentifier */])(start) ? start : end;
    },

    days() {
      return Object(util_timestamp["f" /* createDayList */])(this.parsedStart, this.parsedEnd, this.times.today, this.weekdaySkips);
    },

    dayFormatter() {
      if (this.dayFormat) {
        return this.dayFormat;
      }

      const options = {
        timeZone: 'UTC',
        day: 'numeric'
      };
      return Object(util_timestamp["h" /* createNativeLocaleFormatter */])(this.currentLocale, (_tms, _short) => options);
    },

    weekdayFormatter() {
      if (this.weekdayFormat) {
        return this.weekdayFormat;
      }

      const longOptions = {
        timeZone: 'UTC',
        weekday: 'long'
      };
      const shortOptions = {
        timeZone: 'UTC',
        weekday: 'short'
      };
      return Object(util_timestamp["h" /* createNativeLocaleFormatter */])(this.currentLocale, (_tms, short) => short ? shortOptions : longOptions);
    }

  },
  methods: {
    getRelativeClasses(timestamp, outside = false) {
      return {
        'v-present': timestamp.present,
        'v-past': timestamp.past,
        'v-future': timestamp.future,
        'v-outside': outside
      };
    },

    getStartOfWeek(timestamp) {
      return Object(util_timestamp["o" /* getStartOfWeek */])(timestamp, this.parsedWeekdays, this.times.today);
    },

    getEndOfWeek(timestamp) {
      return Object(util_timestamp["m" /* getEndOfWeek */])(timestamp, this.parsedWeekdays, this.times.today);
    },

    getFormatter(options) {
      return Object(util_timestamp["h" /* createNativeLocaleFormatter */])(this.locale, (_tms, _short) => options);
    }

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/helpers.js
var helpers = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/util/events.js

function parseEvent(input, index, startProperty, endProperty, timed = false, category = false) {
  const startInput = input[startProperty];
  const endInput = input[endProperty];
  const startParsed = Object(util_timestamp["v" /* parseTimestamp */])(startInput, true);
  const endParsed = endInput ? Object(util_timestamp["v" /* parseTimestamp */])(endInput, true) : startParsed;
  const start = Object(util_timestamp["r" /* isTimedless */])(startInput) ? Object(util_timestamp["A" /* updateHasTime */])(startParsed, timed) : startParsed;
  const end = Object(util_timestamp["r" /* isTimedless */])(endInput) ? Object(util_timestamp["A" /* updateHasTime */])(endParsed, timed) : endParsed;
  const startIdentifier = Object(util_timestamp["k" /* getDayIdentifier */])(start);
  const startTimestampIdentifier = Object(util_timestamp["p" /* getTimestampIdentifier */])(start);
  const endIdentifier = Object(util_timestamp["k" /* getDayIdentifier */])(end);
  const endOffset = start.hasTime ? 0 : 2359;
  const endTimestampIdentifier = Object(util_timestamp["p" /* getTimestampIdentifier */])(end) + endOffset;
  const allDay = !start.hasTime;
  return {
    input,
    start,
    startIdentifier,
    startTimestampIdentifier,
    end,
    endIdentifier,
    endTimestampIdentifier,
    allDay,
    index,
    category
  };
}
function isEventOn(event, dayIdentifier) {
  return dayIdentifier >= event.startIdentifier && dayIdentifier <= event.endIdentifier;
}
function isEventHiddenOn(event, day) {
  return event.end.time === '00:00' && event.end.date === day.date && event.start.date !== day.date;
}
function isEventStart(event, day, dayIdentifier, firstWeekday) {
  return dayIdentifier === event.startIdentifier || firstWeekday === day.weekday && isEventOn(event, dayIdentifier);
}
function isEventOverlapping(event, startIdentifier, endIdentifier) {
  return startIdentifier <= event.endIdentifier && endIdentifier >= event.startIdentifier;
}
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/mixins/calendar-with-events.js
// Styles
 // Directives

 // Mixins

 // Helpers

 // Util





const WIDTH_FULL = 100;
const WIDTH_START = 95;
const MINUTES_IN_DAY = 1440;
/* @vue/component */

/* harmony default export */ var mixins_calendar_with_events = (calendar_base.extend({
  name: 'calendar-with-events',
  directives: {
    ripple: ripple["a" /* default */]
  },
  props: { ...props.events,
    ...props.calendar,
    ...props.category
  },
  computed: {
    noEvents() {
      return this.events.length === 0;
    },

    parsedEvents() {
      return this.events.map(this.parseEvent);
    },

    parsedEventOverlapThreshold() {
      return parseInt(this.eventOverlapThreshold);
    },

    eventTimedFunction() {
      return typeof this.eventTimed === 'function' ? this.eventTimed : event => !!event[this.eventTimed];
    },

    eventCategoryFunction() {
      return typeof this.eventCategory === 'function' ? this.eventCategory : event => event[this.eventCategory];
    },

    eventTextColorFunction() {
      return typeof this.eventTextColor === 'function' ? this.eventTextColor : () => this.eventTextColor;
    },

    eventNameFunction() {
      return typeof this.eventName === 'function' ? this.eventName : (event, timedEvent) => Object(helpers["m" /* escapeHTML */])(event.input[this.eventName] || '');
    },

    eventModeFunction() {
      return typeof this.eventOverlapMode === 'function' ? this.eventOverlapMode : CalendarEventOverlapModes[this.eventOverlapMode];
    },

    eventWeekdays() {
      return this.parsedWeekdays;
    },

    categoryMode() {
      return this.type === 'category';
    }

  },
  methods: {
    eventColorFunction(e) {
      return typeof this.eventColor === 'function' ? this.eventColor(e) : e.color || this.eventColor;
    },

    parseEvent(input, index = 0) {
      return parseEvent(input, index, this.eventStart, this.eventEnd, this.eventTimedFunction(input), this.categoryMode ? this.eventCategoryFunction(input) : false);
    },

    formatTime(withTime, ampm) {
      const formatter = this.getFormatter({
        timeZone: 'UTC',
        hour: 'numeric',
        minute: withTime.minute > 0 ? 'numeric' : undefined
      });
      return formatter(withTime, true);
    },

    updateEventVisibility() {
      if (this.noEvents || !this.eventMore) {
        return;
      }

      const eventHeight = this.eventHeight;
      const eventsMap = this.getEventsMap();

      for (const date in eventsMap) {
        const {
          parent,
          events,
          more
        } = eventsMap[date];

        if (!more) {
          break;
        }

        const parentBounds = parent.getBoundingClientRect();
        const last = events.length - 1;
        const eventsSorted = events.map(event => ({
          event,
          bottom: event.getBoundingClientRect().bottom
        })).sort((a, b) => a.bottom - b.bottom);
        let hidden = 0;

        for (let i = 0; i <= last; i++) {
          const bottom = eventsSorted[i].bottom;
          const hide = i === last ? bottom > parentBounds.bottom : bottom + eventHeight > parentBounds.bottom;

          if (hide) {
            eventsSorted[i].event.style.display = 'none';
            hidden++;
          }
        }

        if (hidden) {
          more.style.display = '';
          more.innerHTML = this.$vuetify.lang.t(this.eventMoreText, hidden);
        } else {
          more.style.display = 'none';
        }
      }
    },

    getEventsMap() {
      const eventsMap = {};
      const elements = this.$refs.events;

      if (!elements || !elements.forEach) {
        return eventsMap;
      }

      elements.forEach(el => {
        const date = el.getAttribute('data-date');

        if (el.parentElement && date) {
          if (!(date in eventsMap)) {
            eventsMap[date] = {
              parent: el.parentElement,
              more: null,
              events: []
            };
          }

          if (el.getAttribute('data-more')) {
            eventsMap[date].more = el;
          } else {
            eventsMap[date].events.push(el);
            el.style.display = '';
          }
        }
      });
      return eventsMap;
    },

    genDayEvent({
      event
    }, day) {
      const eventHeight = this.eventHeight;
      const eventMarginBottom = this.eventMarginBottom;
      const dayIdentifier = Object(util_timestamp["k" /* getDayIdentifier */])(day);
      const week = day.week;
      const start = dayIdentifier === event.startIdentifier;
      let end = dayIdentifier === event.endIdentifier;
      let width = WIDTH_START;

      if (!this.categoryMode) {
        for (let i = day.index + 1; i < week.length; i++) {
          const weekdayIdentifier = Object(util_timestamp["k" /* getDayIdentifier */])(week[i]);

          if (event.endIdentifier >= weekdayIdentifier) {
            width += WIDTH_FULL;
            end = end || weekdayIdentifier === event.endIdentifier;
          } else {
            end = true;
            break;
          }
        }
      }

      const scope = {
        eventParsed: event,
        day,
        start,
        end,
        timed: false
      };
      return this.genEvent(event, scope, false, {
        staticClass: 'v-event',
        class: {
          'v-event-start': start,
          'v-event-end': end
        },
        style: {
          height: `${eventHeight}px`,
          width: `${width}%`,
          'margin-bottom': `${eventMarginBottom}px`
        },
        attrs: {
          'data-date': day.date
        },
        key: event.index,
        ref: 'events',
        refInFor: true
      });
    },

    genTimedEvent({
      event,
      left,
      width
    }, day) {
      if (day.timeDelta(event.end) < 0 || day.timeDelta(event.start) >= 1 || isEventHiddenOn(event, day)) {
        return false;
      }

      const dayIdentifier = Object(util_timestamp["k" /* getDayIdentifier */])(day);
      const start = event.startIdentifier >= dayIdentifier;
      const end = event.endIdentifier > dayIdentifier;
      const top = start ? day.timeToY(event.start) : 0;
      const bottom = end ? day.timeToY(MINUTES_IN_DAY) : day.timeToY(event.end);
      const height = Math.max(this.eventHeight, bottom - top);
      const scope = {
        eventParsed: event,
        day,
        start,
        end,
        timed: true
      };
      return this.genEvent(event, scope, true, {
        staticClass: 'v-event-timed',
        style: {
          top: `${top}px`,
          height: `${height}px`,
          left: `${left}%`,
          width: `${width}%`
        }
      });
    },

    genEvent(event, scopeInput, timedEvent, data) {
      var _this$eventRipple;

      const slot = this.$scopedSlots.event;
      const text = this.eventTextColorFunction(event.input);
      const background = this.eventColorFunction(event.input);
      const overlapsNoon = event.start.hour < 12 && event.end.hour >= 12;
      const singline = Object(util_timestamp["j" /* diffMinutes */])(event.start, event.end) <= this.parsedEventOverlapThreshold;
      const formatTime = this.formatTime;

      const timeSummary = () => formatTime(event.start, overlapsNoon) + ' - ' + formatTime(event.end, true);

      const eventSummary = () => {
        const name = this.eventNameFunction(event, timedEvent);

        if (event.start.hasTime) {
          const eventSummaryClass = 'v-event-summary';

          if (timedEvent) {
            const time = timeSummary();
            const delimiter = singline ? ', ' : '<br>';
            return `<span class="${eventSummaryClass}"><strong>${name}</strong>${delimiter}${time}</span>`;
          } else {
            const time = formatTime(event.start, true);
            return `<span class="${eventSummaryClass}"><strong>${time}</strong> ${name}</span>`;
          }
        }

        return name;
      };

      const scope = { ...scopeInput,
        event: event.input,
        outside: scopeInput.day.outside,
        singline,
        overlapsNoon,
        formatTime,
        timeSummary,
        eventSummary
      };
      return this.$createElement('div', this.setTextColor(text, this.setBackgroundColor(background, {
        on: this.getDefaultMouseEventHandlers(':event', nativeEvent => ({ ...scope,
          nativeEvent
        })),
        directives: [{
          name: 'ripple',
          value: (_this$eventRipple = this.eventRipple) != null ? _this$eventRipple : true
        }],
        ...data
      })), slot ? slot(scope) : [this.genName(eventSummary)]);
    },

    genName(eventSummary) {
      return this.$createElement('div', {
        staticClass: 'pl-1',
        domProps: {
          innerHTML: eventSummary()
        }
      });
    },

    genPlaceholder(day) {
      const height = this.eventHeight + this.eventMarginBottom;
      return this.$createElement('div', {
        style: {
          height: `${height}px`
        },
        attrs: {
          'data-date': day.date
        },
        ref: 'events',
        refInFor: true
      });
    },

    genMore(day) {
      var _this$eventRipple2;

      const eventHeight = this.eventHeight;
      const eventMarginBottom = this.eventMarginBottom;
      return this.$createElement('div', {
        staticClass: 'v-event-more pl-1',
        class: {
          'v-outside': day.outside
        },
        attrs: {
          'data-date': day.date,
          'data-more': 1
        },
        directives: [{
          name: 'ripple',
          value: (_this$eventRipple2 = this.eventRipple) != null ? _this$eventRipple2 : true
        }],
        on: this.getDefaultMouseEventHandlers(':more', nativeEvent => {
          return {
            nativeEvent,
            ...day
          };
        }),
        style: {
          display: 'none',
          height: `${eventHeight}px`,
          'margin-bottom': `${eventMarginBottom}px`
        },
        ref: 'events',
        refInFor: true
      });
    },

    getVisibleEvents() {
      const start = Object(util_timestamp["k" /* getDayIdentifier */])(this.days[0]);
      const end = Object(util_timestamp["k" /* getDayIdentifier */])(this.days[this.days.length - 1]);
      return this.parsedEvents.filter(event => isEventOverlapping(event, start, end));
    },

    isEventForCategory(event, category) {
      return !this.categoryMode || typeof category === 'object' && category.categoryName && category.categoryName === event.category || typeof event.category === 'string' && category === event.category || typeof event.category !== 'string' && category === null;
    },

    getEventsForDay(day) {
      const identifier = Object(util_timestamp["k" /* getDayIdentifier */])(day);
      const firstWeekday = this.eventWeekdays[0];
      return this.parsedEvents.filter(event => isEventStart(event, day, identifier, firstWeekday));
    },

    getEventsForDayAll(day) {
      const identifier = Object(util_timestamp["k" /* getDayIdentifier */])(day);
      const firstWeekday = this.eventWeekdays[0];
      return this.parsedEvents.filter(event => event.allDay && (this.categoryMode ? isEventOn(event, identifier) : isEventStart(event, day, identifier, firstWeekday)) && this.isEventForCategory(event, day.category));
    },

    getEventsForDayTimed(day) {
      const identifier = Object(util_timestamp["k" /* getDayIdentifier */])(day);
      return this.parsedEvents.filter(event => !event.allDay && isEventOn(event, identifier) && this.isEventForCategory(event, day.category));
    },

    getScopedSlots() {
      if (this.noEvents) {
        return { ...this.$scopedSlots
        };
      }

      const mode = this.eventModeFunction(this.parsedEvents, this.eventWeekdays[0], this.parsedEventOverlapThreshold);

      const isNode = input => !!input;

      const getSlotChildren = (day, getter, mapper, timed) => {
        const events = getter(day);
        const visuals = mode(day, events, timed, this.categoryMode);

        if (timed) {
          return visuals.map(visual => mapper(visual, day)).filter(isNode);
        }

        const children = [];
        visuals.forEach((visual, index) => {
          while (children.length < visual.column) {
            children.push(this.genPlaceholder(day));
          }

          const mapped = mapper(visual, day);

          if (mapped) {
            children.push(mapped);
          }
        });
        return children;
      };

      const slots = this.$scopedSlots;
      const slotDay = slots.day;
      const slotDayHeader = slots['day-header'];
      const slotDayBody = slots['day-body'];
      return { ...slots,
        day: day => {
          let children = getSlotChildren(day, this.getEventsForDay, this.genDayEvent, false);

          if (children && children.length > 0 && this.eventMore) {
            children.push(this.genMore(day));
          }

          if (slotDay) {
            const slot = slotDay(day);

            if (slot) {
              children = children ? children.concat(slot) : slot;
            }
          }

          return children;
        },
        'day-header': day => {
          let children = getSlotChildren(day, this.getEventsForDayAll, this.genDayEvent, false);

          if (slotDayHeader) {
            const slot = slotDayHeader(day);

            if (slot) {
              children = children ? children.concat(slot) : slot;
            }
          }

          return children;
        },
        'day-body': day => {
          const events = getSlotChildren(day, this.getEventsForDayTimed, this.genTimedEvent, true);
          let children = [this.$createElement('div', {
            staticClass: 'v-event-timed-container'
          }, events)];

          if (slotDayBody) {
            const slot = slotDayBody(day);

            if (slot) {
              children = children.concat(slot);
            }
          }

          return children;
        }
      };
    }

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VCalendar/VCalendarWeekly.sass
var VCalendarWeekly = __webpack_require__(282);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VBtn/index.js
var components_VBtn = __webpack_require__(41);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/util/dateTimeUtils.js
var dateTimeUtils = __webpack_require__(58);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/VCalendarWeekly.js
// Styles
 // Components

 // Mixins

 // Util





/* @vue/component */

/* harmony default export */ var VCalendar_VCalendarWeekly = (calendar_base.extend({
  name: 'v-calendar-weekly',
  props: props.weeks,
  computed: {
    staticClass() {
      return 'v-calendar-weekly';
    },

    classes() {
      return this.themeClasses;
    },

    parsedMinWeeks() {
      return parseInt(this.minWeeks);
    },

    days() {
      const minDays = this.parsedMinWeeks * this.parsedWeekdays.length;
      const start = this.getStartOfWeek(this.parsedStart);
      const end = this.getEndOfWeek(this.parsedEnd);
      return Object(util_timestamp["f" /* createDayList */])(start, end, this.times.today, this.weekdaySkips, Number.MAX_SAFE_INTEGER, minDays);
    },

    todayWeek() {
      const today = this.times.today;
      const start = this.getStartOfWeek(today);
      const end = this.getEndOfWeek(today);
      return Object(util_timestamp["f" /* createDayList */])(start, end, today, this.weekdaySkips, this.parsedWeekdays.length, this.parsedWeekdays.length);
    },

    monthFormatter() {
      if (this.monthFormat) {
        return this.monthFormat;
      }

      const longOptions = {
        timeZone: 'UTC',
        month: 'long'
      };
      const shortOptions = {
        timeZone: 'UTC',
        month: 'short'
      };
      return Object(util_timestamp["h" /* createNativeLocaleFormatter */])(this.currentLocale, (_tms, short) => short ? shortOptions : longOptions);
    }

  },
  methods: {
    isOutside(day) {
      const dayIdentifier = Object(util_timestamp["k" /* getDayIdentifier */])(day);
      return dayIdentifier < Object(util_timestamp["k" /* getDayIdentifier */])(this.parsedStart) || dayIdentifier > Object(util_timestamp["k" /* getDayIdentifier */])(this.parsedEnd);
    },

    genHead() {
      return this.$createElement('div', {
        staticClass: 'v-calendar-weekly__head'
      }, this.genHeadDays());
    },

    genHeadDays() {
      const header = this.todayWeek.map(this.genHeadDay);

      if (this.showWeek) {
        header.unshift(this.$createElement('div', {
          staticClass: 'v-calendar-weekly__head-weeknumber'
        }));
      }

      return header;
    },

    genHeadDay(day, index) {
      const outside = this.isOutside(this.days[index]);
      const color = day.present ? this.color : undefined;
      return this.$createElement('div', this.setTextColor(color, {
        key: day.date,
        staticClass: 'v-calendar-weekly__head-weekday',
        class: this.getRelativeClasses(day, outside)
      }), this.weekdayFormatter(day, this.shortWeekdays));
    },

    genWeeks() {
      const days = this.days;
      const weekDays = this.parsedWeekdays.length;
      const weeks = [];

      for (let i = 0; i < days.length; i += weekDays) {
        weeks.push(this.genWeek(days.slice(i, i + weekDays), this.getWeekNumber(days[i])));
      }

      return weeks;
    },

    genWeek(week, weekNumber) {
      const weekNodes = week.map((day, index) => this.genDay(day, index, week));

      if (this.showWeek) {
        weekNodes.unshift(this.genWeekNumber(weekNumber));
      }

      return this.$createElement('div', {
        key: week[0].date,
        staticClass: 'v-calendar-weekly__week'
      }, weekNodes);
    },

    getWeekNumber(determineDay) {
      return Object(dateTimeUtils["b" /* weekNumber */])(determineDay.year, determineDay.month - 1, determineDay.day, this.parsedWeekdays[0], parseInt(this.localeFirstDayOfYear));
    },

    genWeekNumber(weekNumber) {
      return this.$createElement('div', {
        staticClass: 'v-calendar-weekly__weeknumber'
      }, [this.$createElement('small', String(weekNumber))]);
    },

    genDay(day, index, week) {
      const outside = this.isOutside(day);
      return this.$createElement('div', {
        key: day.date,
        staticClass: 'v-calendar-weekly__day',
        class: this.getRelativeClasses(day, outside),
        on: this.getDefaultMouseEventHandlers(':day', nativeEvent => {
          return {
            nativeEvent,
            ...day
          };
        })
      }, [this.genDayLabel(day), ...(Object(helpers["t" /* getSlot */])(this, 'day', () => ({
        outside,
        index,
        week,
        ...day
      })) || [])]);
    },

    genDayLabel(day) {
      return this.$createElement('div', {
        staticClass: 'v-calendar-weekly__day-label'
      }, Object(helpers["t" /* getSlot */])(this, 'day-label', day) || [this.genDayLabelButton(day)]);
    },

    genDayLabelButton(day) {
      const color = day.present ? this.color : 'transparent';
      const hasMonth = day.day === 1 && this.showMonthOnFirst;
      return this.$createElement(components_VBtn["a" /* default */], {
        props: {
          color,
          fab: true,
          depressed: true,
          small: true
        },
        on: this.getMouseEventHandlers({
          'click:date': {
            event: 'click',
            stop: true
          },
          'contextmenu:date': {
            event: 'contextmenu',
            stop: true,
            prevent: true,
            result: false
          }
        }, nativeEvent => ({
          nativeEvent,
          ...day
        }))
      }, hasMonth ? this.monthFormatter(day, this.shortMonths) + ' ' + this.dayFormatter(day, false) : this.dayFormatter(day, false));
    },

    genDayMonth(day) {
      const color = day.present ? this.color : undefined;
      return this.$createElement('div', this.setTextColor(color, {
        staticClass: 'v-calendar-weekly__day-month'
      }), Object(helpers["t" /* getSlot */])(this, 'day-month', day) || this.monthFormatter(day, this.shortMonths));
    }

  },

  render(h) {
    return h('div', {
      staticClass: this.staticClass,
      class: this.classes,
      on: {
        dragstart: e => {
          e.preventDefault();
        }
      }
    }, [!this.hideHeader ? this.genHead() : '', ...this.genWeeks()]);
  }

}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/VCalendarMonthly.js
// Styles
 // Mixins

 // Util


/* @vue/component */

/* harmony default export */ var VCalendarMonthly = (VCalendar_VCalendarWeekly.extend({
  name: 'v-calendar-monthly',
  computed: {
    staticClass() {
      return 'v-calendar-monthly v-calendar-weekly';
    },

    parsedStart() {
      return Object(util_timestamp["n" /* getStartOfMonth */])(Object(util_timestamp["v" /* parseTimestamp */])(this.start, true));
    },

    parsedEnd() {
      return Object(util_timestamp["l" /* getEndOfMonth */])(Object(util_timestamp["v" /* parseTimestamp */])(this.end, true));
    }

  }
}));
// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VCalendar/VCalendarDaily.sass
var VCalendarDaily = __webpack_require__(301);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/mixins/calendar-with-intervals.js
// Mixins
 // Util



/* @vue/component */

/* harmony default export */ var calendar_with_intervals = (calendar_base.extend({
  name: 'calendar-with-intervals',
  props: props.intervals,
  computed: {
    parsedFirstInterval() {
      return parseInt(this.firstInterval);
    },

    parsedIntervalMinutes() {
      return parseInt(this.intervalMinutes);
    },

    parsedIntervalCount() {
      return parseInt(this.intervalCount);
    },

    parsedIntervalHeight() {
      return parseFloat(this.intervalHeight);
    },

    parsedFirstTime() {
      return Object(util_timestamp["u" /* parseTime */])(this.firstTime);
    },

    firstMinute() {
      const time = this.parsedFirstTime;
      return time !== false && time >= 0 && time <= util_timestamp["d" /* MINUTES_IN_DAY */] ? time : this.parsedFirstInterval * this.parsedIntervalMinutes;
    },

    bodyHeight() {
      return this.parsedIntervalCount * this.parsedIntervalHeight;
    },

    days() {
      return Object(util_timestamp["f" /* createDayList */])(this.parsedStart, this.parsedEnd, this.times.today, this.weekdaySkips, this.maxDays);
    },

    intervals() {
      const days = this.days;
      const first = this.firstMinute;
      const minutes = this.parsedIntervalMinutes;
      const count = this.parsedIntervalCount;
      const now = this.times.now;
      return days.map(d => Object(util_timestamp["g" /* createIntervalList */])(d, first, minutes, count, now));
    },

    intervalFormatter() {
      if (this.intervalFormat) {
        return this.intervalFormat;
      }

      const longOptions = {
        timeZone: 'UTC',
        hour: '2-digit',
        minute: '2-digit'
      };
      const shortOptions = {
        timeZone: 'UTC',
        hour: 'numeric',
        minute: '2-digit'
      };
      const shortHourOptions = {
        timeZone: 'UTC',
        hour: 'numeric'
      };
      return Object(util_timestamp["h" /* createNativeLocaleFormatter */])(this.currentLocale, (tms, short) => short ? tms.minute === 0 ? shortHourOptions : shortOptions : longOptions);
    }

  },
  methods: {
    showIntervalLabelDefault(interval) {
      const first = this.intervals[0][0];
      const isFirst = first.hour === interval.hour && first.minute === interval.minute;
      return !isFirst;
    },

    intervalStyleDefault(_interval) {
      return undefined;
    },

    getTimestampAtEvent(e, day) {
      const timestamp = Object(util_timestamp["e" /* copyTimestamp */])(day);
      const bounds = e.currentTarget.getBoundingClientRect();
      const baseMinutes = this.firstMinute;
      const touchEvent = e;
      const mouseEvent = e;
      const touches = touchEvent.changedTouches || touchEvent.touches;
      const clientY = touches && touches[0] ? touches[0].clientY : mouseEvent.clientY;
      const addIntervals = (clientY - bounds.top) / this.parsedIntervalHeight;
      const addMinutes = Math.floor(addIntervals * this.parsedIntervalMinutes);
      const minutes = baseMinutes + addMinutes;
      return Object(util_timestamp["B" /* updateMinutes */])(timestamp, minutes, this.times.now);
    },

    getSlotScope(timestamp) {
      const scope = Object(util_timestamp["e" /* copyTimestamp */])(timestamp);
      scope.timeToY = this.timeToY;
      scope.timeDelta = this.timeDelta;
      scope.minutesToPixels = this.minutesToPixels;
      scope.week = this.days;
      return scope;
    },

    scrollToTime(time) {
      const y = this.timeToY(time);
      const pane = this.$refs.scrollArea;

      if (y === false || !pane) {
        return false;
      }

      pane.scrollTop = y;
      return true;
    },

    minutesToPixels(minutes) {
      return minutes / this.parsedIntervalMinutes * this.parsedIntervalHeight;
    },

    timeToY(time, clamp = true) {
      let y = this.timeDelta(time);

      if (y !== false) {
        y *= this.bodyHeight;

        if (clamp) {
          if (y < 0) {
            y = 0;
          }

          if (y > this.bodyHeight) {
            y = this.bodyHeight;
          }
        }
      }

      return y;
    },

    timeDelta(time) {
      const minutes = Object(util_timestamp["u" /* parseTime */])(time);

      if (minutes === false) {
        return false;
      }

      const min = this.firstMinute;
      const gap = this.parsedIntervalCount * this.parsedIntervalMinutes;
      return (minutes - min) / gap;
    }

  }
}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/VCalendarDaily.js
// Styles
 // Directives

 // Components

 // Mixins

 // Util


/* @vue/component */

/* harmony default export */ var VCalendar_VCalendarDaily = (calendar_with_intervals.extend({
  name: 'v-calendar-daily',
  directives: {
    Resize: resize["a" /* default */]
  },
  data: () => ({
    scrollPush: 0
  }),
  computed: {
    classes() {
      return {
        'v-calendar-daily': true,
        ...this.themeClasses
      };
    }

  },

  mounted() {
    this.init();
  },

  methods: {
    init() {
      this.$nextTick(this.onResize);
    },

    onResize() {
      this.scrollPush = this.getScrollPush();
    },

    getScrollPush() {
      const area = this.$refs.scrollArea;
      const pane = this.$refs.pane;
      return area && pane ? area.offsetWidth - pane.offsetWidth : 0;
    },

    genHead() {
      return this.$createElement('div', {
        staticClass: 'v-calendar-daily__head',
        style: {
          marginRight: this.scrollPush + 'px'
        }
      }, [this.genHeadIntervals(), ...this.genHeadDays()]);
    },

    genHeadIntervals() {
      const width = Object(helpers["h" /* convertToUnit */])(this.intervalWidth);
      return this.$createElement('div', {
        staticClass: 'v-calendar-daily__intervals-head',
        style: {
          width
        }
      }, Object(helpers["t" /* getSlot */])(this, 'interval-header'));
    },

    genHeadDays() {
      return this.days.map(this.genHeadDay);
    },

    genHeadDay(day, index) {
      return this.$createElement('div', {
        key: day.date,
        staticClass: 'v-calendar-daily_head-day',
        class: this.getRelativeClasses(day),
        on: this.getDefaultMouseEventHandlers(':day', nativeEvent => {
          return {
            nativeEvent,
            ...this.getSlotScope(day)
          };
        })
      }, [this.genHeadWeekday(day), this.genHeadDayLabel(day), ...this.genDayHeader(day, index)]);
    },

    genDayHeader(day, index) {
      return Object(helpers["t" /* getSlot */])(this, 'day-header', () => ({
        week: this.days,
        ...day,
        index
      })) || [];
    },

    genHeadWeekday(day) {
      const color = day.present ? this.color : undefined;
      return this.$createElement('div', this.setTextColor(color, {
        staticClass: 'v-calendar-daily_head-weekday'
      }), this.weekdayFormatter(day, this.shortWeekdays));
    },

    genHeadDayLabel(day) {
      return this.$createElement('div', {
        staticClass: 'v-calendar-daily_head-day-label'
      }, Object(helpers["t" /* getSlot */])(this, 'day-label-header', day) || [this.genHeadDayButton(day)]);
    },

    genHeadDayButton(day) {
      const color = day.present ? this.color : 'transparent';
      return this.$createElement(components_VBtn["a" /* default */], {
        props: {
          color,
          fab: true,
          depressed: true
        },
        on: this.getMouseEventHandlers({
          'click:date': {
            event: 'click',
            stop: true
          },
          'contextmenu:date': {
            event: 'contextmenu',
            stop: true,
            prevent: true,
            result: false
          }
        }, nativeEvent => {
          return {
            nativeEvent,
            ...day
          };
        })
      }, this.dayFormatter(day, false));
    },

    genBody() {
      return this.$createElement('div', {
        staticClass: 'v-calendar-daily__body'
      }, [this.genScrollArea()]);
    },

    genScrollArea() {
      return this.$createElement('div', {
        ref: 'scrollArea',
        staticClass: 'v-calendar-daily__scroll-area'
      }, [this.genPane()]);
    },

    genPane() {
      return this.$createElement('div', {
        ref: 'pane',
        staticClass: 'v-calendar-daily__pane',
        style: {
          height: Object(helpers["h" /* convertToUnit */])(this.bodyHeight)
        }
      }, [this.genDayContainer()]);
    },

    genDayContainer() {
      return this.$createElement('div', {
        staticClass: 'v-calendar-daily__day-container'
      }, [this.genBodyIntervals(), ...this.genDays()]);
    },

    genDays() {
      return this.days.map(this.genDay);
    },

    genDay(day, index) {
      return this.$createElement('div', {
        key: day.date,
        staticClass: 'v-calendar-daily__day',
        class: this.getRelativeClasses(day),
        on: this.getDefaultMouseEventHandlers(':time', nativeEvent => {
          return {
            nativeEvent,
            ...this.getSlotScope(this.getTimestampAtEvent(nativeEvent, day))
          };
        })
      }, [...this.genDayIntervals(index), ...this.genDayBody(day)]);
    },

    genDayBody(day) {
      return Object(helpers["t" /* getSlot */])(this, 'day-body', () => this.getSlotScope(day)) || [];
    },

    genDayIntervals(index) {
      return this.intervals[index].map(this.genDayInterval);
    },

    genDayInterval(interval) {
      const height = Object(helpers["h" /* convertToUnit */])(this.intervalHeight);
      const styler = this.intervalStyle || this.intervalStyleDefault;
      const data = {
        key: interval.time,
        staticClass: 'v-calendar-daily__day-interval',
        style: {
          height,
          ...styler(interval)
        }
      };
      const children = Object(helpers["t" /* getSlot */])(this, 'interval', () => this.getSlotScope(interval));
      return this.$createElement('div', data, children);
    },

    genBodyIntervals() {
      const width = Object(helpers["h" /* convertToUnit */])(this.intervalWidth);
      const data = {
        staticClass: 'v-calendar-daily__intervals-body',
        style: {
          width
        },
        on: this.getDefaultMouseEventHandlers(':interval', nativeEvent => {
          return {
            nativeEvent,
            ...this.getTimestampAtEvent(nativeEvent, this.parsedStart)
          };
        })
      };
      return this.$createElement('div', data, this.genIntervalLabels());
    },

    genIntervalLabels() {
      if (!this.intervals.length) return null;
      return this.intervals[0].map(this.genIntervalLabel);
    },

    genIntervalLabel(interval) {
      const height = Object(helpers["h" /* convertToUnit */])(this.intervalHeight);
      const short = this.shortIntervals;
      const shower = this.showIntervalLabel || this.showIntervalLabelDefault;
      const show = shower(interval);
      const label = show ? this.intervalFormatter(interval, short) : undefined;
      return this.$createElement('div', {
        key: interval.time,
        staticClass: 'v-calendar-daily__interval',
        style: {
          height
        }
      }, [this.$createElement('div', {
        staticClass: 'v-calendar-daily__interval-text'
      }, label)]);
    }

  },

  render(h) {
    return h('div', {
      class: this.classes,
      on: {
        dragstart: e => {
          e.preventDefault();
        }
      },
      directives: [{
        modifiers: {
          quiet: true
        },
        name: 'resize',
        value: this.onResize
      }]
    }, [!this.hideHeader ? this.genHead() : '', this.genBody()]);
  }

}));
// EXTERNAL MODULE: ./node_modules/vuetify/src/components/VCalendar/VCalendarCategory.sass
var VCalendarCategory = __webpack_require__(303);

// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/util/parser.js
function parsedCategoryText(category, categoryText) {
  return typeof categoryText === 'string' && typeof category === 'object' && category ? category[categoryText] : typeof categoryText === 'function' ? categoryText(category) : category;
}
function getParsedCategories(categories, categoryText) {
  if (typeof categories === 'string') return categories.split(/\s*,\s/);

  if (Array.isArray(categories)) {
    return categories.map(category => {
      if (typeof category === 'string') return category;
      const categoryName = typeof category.categoryName === 'string' ? category.categoryName : parsedCategoryText(category, categoryText);
      return { ...category,
        categoryName
      };
    });
  }

  return [];
}
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/VCalendarCategory.js
// Styles
 // Mixins

 // Util




/* @vue/component */

/* harmony default export */ var VCalendar_VCalendarCategory = (VCalendar_VCalendarDaily.extend({
  name: 'v-calendar-category',
  props: props.category,
  computed: {
    classes() {
      return {
        'v-calendar-daily': true,
        'v-calendar-category': true,
        ...this.themeClasses
      };
    },

    parsedCategories() {
      return getParsedCategories(this.categories, this.categoryText);
    }

  },
  methods: {
    genDayHeader(day, index) {
      const data = {
        staticClass: 'v-calendar-category__columns'
      };
      const scope = {
        week: this.days,
        ...day,
        index
      };
      const children = this.parsedCategories.map(category => {
        return this.genDayHeaderCategory(day, this.getCategoryScope(scope, category));
      });
      return [this.$createElement('div', data, children)];
    },

    getCategoryScope(scope, category) {
      const cat = typeof category === 'object' && category && category.categoryName === this.categoryForInvalid ? null : category;
      return { ...scope,
        category: cat
      };
    },

    genDayHeaderCategory(day, scope) {
      const headerTitle = typeof scope.category === 'object' ? scope.category.categoryName : scope.category;
      return this.$createElement('div', {
        staticClass: 'v-calendar-category__column-header',
        on: this.getDefaultMouseEventHandlers(':day-category', e => {
          return this.getCategoryScope(this.getSlotScope(day), scope.category);
        })
      }, [Object(helpers["t" /* getSlot */])(this, 'category', scope) || this.genDayHeaderCategoryTitle(headerTitle), Object(helpers["t" /* getSlot */])(this, 'day-header', scope)]);
    },

    genDayHeaderCategoryTitle(categoryName) {
      return this.$createElement('div', {
        staticClass: 'v-calendar-category__category'
      }, categoryName === null ? this.categoryForInvalid : categoryName);
    },

    genDays() {
      const days = [];
      this.days.forEach((d, j) => {
        const day = new Array(this.parsedCategories.length || 1);
        day.fill(d);
        days.push(...day.map((v, i) => this.genDay(v, j, i)));
      });
      return days;
    },

    genDay(day, index, categoryIndex) {
      const category = this.parsedCategories[categoryIndex];
      return this.$createElement('div', {
        key: day.date + '-' + categoryIndex,
        staticClass: 'v-calendar-daily__day',
        class: this.getRelativeClasses(day),
        on: this.getDefaultMouseEventHandlers(':time', e => {
          return this.getSlotScope(this.getTimestampAtEvent(e, day));
        })
      }, [...this.genDayIntervals(index, category), ...this.genDayBody(day, category)]);
    },

    genDayIntervals(index, category) {
      return this.intervals[index].map(v => this.genDayInterval(v, category));
    },

    genDayInterval(interval, category) {
      const height = Object(helpers["h" /* convertToUnit */])(this.intervalHeight);
      const styler = this.intervalStyle || this.intervalStyleDefault;
      const data = {
        key: interval.time,
        staticClass: 'v-calendar-daily__day-interval',
        style: {
          height,
          ...styler({ ...interval,
            category
          })
        }
      };
      const children = Object(helpers["t" /* getSlot */])(this, 'interval', () => this.getCategoryScope(this.getSlotScope(interval), category));
      return this.$createElement('div', data, children);
    },

    genDayBody(day, category) {
      const data = {
        staticClass: 'v-calendar-category__columns'
      };
      const children = [this.genDayBodyCategory(day, category)];
      return [this.$createElement('div', data, children)];
    },

    genDayBodyCategory(day, category) {
      const data = {
        staticClass: 'v-calendar-category__column',
        on: this.getDefaultMouseEventHandlers(':time-category', e => {
          return this.getCategoryScope(this.getSlotScope(this.getTimestampAtEvent(e, day)), category);
        })
      };
      const children = Object(helpers["t" /* getSlot */])(this, 'day-body', () => this.getCategoryScope(this.getSlotScope(day), category));
      return this.$createElement('div', data, children);
    }

  }
}));
// CONCATENATED MODULE: ./node_modules/vuetify/lib/components/VCalendar/VCalendar.js
// Styles
// import '../../stylus/components/_calendar-daily.styl'
// Mixins
 // Util


 // Calendars






/* @vue/component */

/* harmony default export */ var VCalendar = (mixins_calendar_with_events.extend({
  name: 'v-calendar',
  props: { ...props.calendar,
    ...props.weeks,
    ...props.intervals,
    ...props.category
  },
  data: () => ({
    lastStart: null,
    lastEnd: null
  }),
  computed: {
    parsedValue() {
      return Object(util_timestamp["F" /* validateTimestamp */])(this.value) ? Object(util_timestamp["v" /* parseTimestamp */])(this.value, true) : this.parsedStart || this.times.today;
    },

    parsedCategoryDays() {
      return parseInt(this.categoryDays) || 1;
    },

    renderProps() {
      const around = this.parsedValue;
      let component = null;
      let maxDays = this.maxDays;
      let weekdays = this.parsedWeekdays;
      let categories = this.parsedCategories;
      let start = around;
      let end = around;

      switch (this.type) {
        case 'month':
          component = VCalendarMonthly;
          start = Object(util_timestamp["n" /* getStartOfMonth */])(around);
          end = Object(util_timestamp["l" /* getEndOfMonth */])(around);
          break;

        case 'week':
          component = VCalendar_VCalendarDaily;
          start = this.getStartOfWeek(around);
          end = this.getEndOfWeek(around);
          maxDays = 7;
          break;

        case 'day':
          component = VCalendar_VCalendarDaily;
          maxDays = 1;
          weekdays = [start.weekday];
          break;

        case '4day':
          component = VCalendar_VCalendarDaily;
          end = Object(util_timestamp["x" /* relativeDays */])(Object(util_timestamp["e" /* copyTimestamp */])(end), util_timestamp["s" /* nextDay */], 3);
          Object(util_timestamp["z" /* updateFormatted */])(end);
          maxDays = 4;
          weekdays = [start.weekday, (start.weekday + 1) % 7, (start.weekday + 2) % 7, (start.weekday + 3) % 7];
          break;

        case 'custom-weekly':
          component = VCalendar_VCalendarWeekly;
          start = this.parsedStart || around;
          end = this.parsedEnd;
          break;

        case 'custom-daily':
          component = VCalendar_VCalendarDaily;
          start = this.parsedStart || around;
          end = this.parsedEnd;
          break;

        case 'category':
          const days = this.parsedCategoryDays;
          component = VCalendar_VCalendarCategory;
          end = Object(util_timestamp["x" /* relativeDays */])(Object(util_timestamp["e" /* copyTimestamp */])(end), util_timestamp["s" /* nextDay */], days);
          Object(util_timestamp["z" /* updateFormatted */])(end);
          maxDays = days;
          weekdays = [];

          for (let i = 0; i < days; i++) {
            weekdays.push((start.weekday + i) % 7);
          }

          categories = this.getCategoryList(categories);
          break;

        default:
          throw new Error(this.type + ' is not a valid Calendar type');
      }

      return {
        component,
        start,
        end,
        maxDays,
        weekdays,
        categories
      };
    },

    eventWeekdays() {
      return this.renderProps.weekdays;
    },

    categoryMode() {
      return this.type === 'category';
    },

    title() {
      const {
        start,
        end
      } = this.renderProps;
      const spanYears = start.year !== end.year;
      const spanMonths = spanYears || start.month !== end.month;

      if (spanYears) {
        return this.monthShortFormatter(start, true) + ' ' + start.year + ' - ' + this.monthShortFormatter(end, true) + ' ' + end.year;
      }

      if (spanMonths) {
        return this.monthShortFormatter(start, true) + ' - ' + this.monthShortFormatter(end, true) + ' ' + end.year;
      } else {
        return this.monthLongFormatter(start, false) + ' ' + start.year;
      }
    },

    monthLongFormatter() {
      return this.getFormatter({
        timeZone: 'UTC',
        month: 'long'
      });
    },

    monthShortFormatter() {
      return this.getFormatter({
        timeZone: 'UTC',
        month: 'short'
      });
    },

    parsedCategories() {
      return getParsedCategories(this.categories, this.categoryText);
    }

  },
  watch: {
    renderProps: 'checkChange'
  },

  mounted() {
    this.updateEventVisibility();
    this.checkChange();
  },

  updated() {
    window.requestAnimationFrame(this.updateEventVisibility);
  },

  methods: {
    checkChange() {
      const {
        lastStart,
        lastEnd
      } = this;
      const {
        start,
        end
      } = this.renderProps;

      if (!lastStart || !lastEnd || start.date !== lastStart.date || end.date !== lastEnd.date) {
        this.lastStart = start;
        this.lastEnd = end;
        this.$emit('change', {
          start,
          end
        });
      }
    },

    move(amount = 1) {
      const moved = Object(util_timestamp["e" /* copyTimestamp */])(this.parsedValue);
      const forward = amount > 0;
      const mover = forward ? util_timestamp["s" /* nextDay */] : util_timestamp["w" /* prevDay */];
      const limit = forward ? util_timestamp["a" /* DAYS_IN_MONTH_MAX */] : util_timestamp["c" /* DAY_MIN */];
      let times = forward ? amount : -amount;

      while (--times >= 0) {
        switch (this.type) {
          case 'month':
            moved.day = limit;
            mover(moved);
            break;

          case 'week':
            Object(util_timestamp["x" /* relativeDays */])(moved, mover, util_timestamp["b" /* DAYS_IN_WEEK */]);
            break;

          case 'day':
            Object(util_timestamp["x" /* relativeDays */])(moved, mover, 1);
            break;

          case '4day':
            Object(util_timestamp["x" /* relativeDays */])(moved, mover, 4);
            break;

          case 'category':
            Object(util_timestamp["x" /* relativeDays */])(moved, mover, this.parsedCategoryDays);
            break;
        }
      }

      Object(util_timestamp["D" /* updateWeekday */])(moved);
      Object(util_timestamp["z" /* updateFormatted */])(moved);
      Object(util_timestamp["C" /* updateRelative */])(moved, this.times.now);

      if (this.value instanceof Date) {
        this.$emit('input', Object(util_timestamp["y" /* timestampToDate */])(moved));
      } else if (typeof this.value === 'number') {
        this.$emit('input', Object(util_timestamp["y" /* timestampToDate */])(moved).getTime());
      } else {
        this.$emit('input', moved.date);
      }

      this.$emit('moved', moved);
    },

    next(amount = 1) {
      this.move(amount);
    },

    prev(amount = 1) {
      this.move(-amount);
    },

    timeToY(time, clamp = true) {
      const c = this.$children[0];

      if (c && c.timeToY) {
        return c.timeToY(time, clamp);
      } else {
        return false;
      }
    },

    timeDelta(time) {
      const c = this.$children[0];

      if (c && c.timeDelta) {
        return c.timeDelta(time);
      } else {
        return false;
      }
    },

    minutesToPixels(minutes) {
      const c = this.$children[0];

      if (c && c.minutesToPixels) {
        return c.minutesToPixels(minutes);
      } else {
        return -1;
      }
    },

    scrollToTime(time) {
      const c = this.$children[0];

      if (c && c.scrollToTime) {
        return c.scrollToTime(time);
      } else {
        return false;
      }
    },

    parseTimestamp(input, required) {
      return Object(util_timestamp["v" /* parseTimestamp */])(input, required, this.times.now);
    },

    timestampToDate(timestamp) {
      return Object(util_timestamp["y" /* timestampToDate */])(timestamp);
    },

    getCategoryList(categories) {
      if (!this.noEvents) {
        const categoryMap = categories.reduce((map, category, index) => {
          if (typeof category === 'object' && category.categoryName) map[category.categoryName] = {
            index,
            count: 0
          };else if (typeof category === 'string') map[category] = {
            index,
            count: 0
          };
          return map;
        }, {});

        if (!this.categoryHideDynamic || !this.categoryShowAll) {
          let categoryLength = categories.length;
          this.parsedEvents.forEach(ev => {
            let category = ev.category;

            if (typeof category !== 'string') {
              category = this.categoryForInvalid;
            }

            if (!category) {
              return;
            }

            if (category in categoryMap) {
              categoryMap[category].count++;
            } else if (!this.categoryHideDynamic) {
              categoryMap[category] = {
                index: categoryLength++,
                count: 1
              };
            }
          });
        }

        if (!this.categoryShowAll) {
          for (const category in categoryMap) {
            if (categoryMap[category].count === 0) {
              delete categoryMap[category];
            }
          }
        }

        categories = categories.filter(category => {
          if (typeof category === 'object' && category.categoryName) {
            return categoryMap.hasOwnProperty(category.categoryName);
          } else if (typeof category === 'string') {
            return categoryMap.hasOwnProperty(category);
          }

          return false;
        });
      }

      return categories;
    }

  },

  render(h) {
    const {
      start,
      end,
      maxDays,
      component,
      weekdays,
      categories
    } = this.renderProps;
    return h(component, {
      staticClass: 'v-calendar',
      class: {
        'v-calendar-events': !this.noEvents
      },
      props: { ...this.$props,
        start: start.date,
        end: end.date,
        maxDays,
        weekdays,
        categories
      },
      directives: [{
        modifiers: {
          quiet: true
        },
        name: 'resize',
        value: this.updateEventVisibility
      }],
      on: { ...this.$listeners,
        'click:date': (day, e) => {
          if (this.$listeners.input) {
            this.$emit('input', day.date);
          }

          if (this.$listeners['click:date']) {
            this.$emit('click:date', day, e);
          }
        }
      },
      scopedSlots: this.getScopedSlots()
    });
  }

}));
// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/VCard.js
var VCard = __webpack_require__(73);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VCard/index.js
var components_VCard = __webpack_require__(29);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(235);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VContainer.js
var VContainer = __webpack_require__(236);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VIcon/VIcon.js
var VIcon = __webpack_require__(71);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VList.js
var VList = __webpack_require__(74);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/VListItem.js
var VListItem = __webpack_require__(45);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VList/index.js + 3 modules
var components_VList = __webpack_require__(12);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VMenu/VMenu.js
var VMenu = __webpack_require__(226);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(237);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VSheet/VSheet.js
var VSheet = __webpack_require__(15);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VSpacer.js
var VSpacer = __webpack_require__(238);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VToolbar/VToolbar.js
var VToolbar = __webpack_require__(16);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VToolbar/index.js
var components_VToolbar = __webpack_require__(80);

// CONCATENATED MODULE: ./pages/inicio.vue





/* normalize component */

var inicio_component = Object(componentNormalizer["a" /* default */])(
  pages_iniciovue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "7ae132bf"
  
)

/* harmony default export */ var inicio = __webpack_exports__["default"] = (inicio_component.exports);

/* vuetify-loader */



















installComponents_default()(inicio_component, {VAlert: VAlert["a" /* default */],VBtn: VBtn["a" /* default */],VCalendar: VCalendar,VCard: VCard["a" /* default */],VCardActions: components_VCard["a" /* VCardActions */],VCardText: components_VCard["b" /* VCardText */],VCol: VCol["a" /* default */],VContainer: VContainer["a" /* default */],VIcon: VIcon["a" /* default */],VList: VList["a" /* default */],VListItem: VListItem["a" /* default */],VListItemTitle: components_VList["b" /* VListItemTitle */],VMenu: VMenu["a" /* default */],VRow: VRow["a" /* default */],VSheet: VSheet["a" /* default */],VSpacer: VSpacer["a" /* default */],VToolbar: VToolbar["a" /* default */],VToolbarTitle: components_VToolbar["a" /* VToolbarTitle */]})


/***/ })

};;
//# sourceMappingURL=inicio.js.map