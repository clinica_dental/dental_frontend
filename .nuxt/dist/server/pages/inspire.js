exports.ids = [24];
exports.modules = {

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/inspire.vue?vue&type=template&id=3d01bc61&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-row',[_c('v-col',{staticClass:"text-center"},[_c('img',{staticClass:"mb-5",attrs:{"src":"/v.png","alt":"Vuetify.js"}}),_vm._v(" "),_c('blockquote',{staticClass:"blockquote"},[_vm._v("\n      “First, solve the problem. Then, write the code.”\n      "),_c('footer',[_c('small',[_c('em',[_vm._v("—John Johnson")])])])])])],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/inspire.vue?vue&type=template&id=3d01bc61&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/inspire.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var inspirevue_type_script_lang_js_ = ({
  name: 'InspirePage'
});
// CONCATENATED MODULE: ./pages/inspire.vue?vue&type=script&lang=js&
 /* harmony default export */ var pages_inspirevue_type_script_lang_js_ = (inspirevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(8);

// EXTERNAL MODULE: ./node_modules/vuetify-loader/lib/runtime/installComponents.js
var installComponents = __webpack_require__(9);
var installComponents_default = /*#__PURE__*/__webpack_require__.n(installComponents);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VCol.js
var VCol = __webpack_require__(231);

// EXTERNAL MODULE: ./node_modules/vuetify/lib/components/VGrid/VRow.js
var VRow = __webpack_require__(233);

// CONCATENATED MODULE: ./pages/inspire.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  pages_inspirevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "0b34b720"
  
)

/* harmony default export */ var inspire = __webpack_exports__["default"] = (component.exports);

/* vuetify-loader */



installComponents_default()(component, {VCol: VCol["a" /* default */],VRow: VRow["a" /* default */]})


/***/ })

};;
//# sourceMappingURL=inspire.js.map