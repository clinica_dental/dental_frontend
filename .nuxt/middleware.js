const middleware = {}

middleware['i18n'] = require('../middleware/i18n.js')
middleware['i18n'] = middleware['i18n'].default || middleware['i18n']

middleware['rutas'] = require('../middleware/rutas.js')
middleware['rutas'] = middleware['rutas'].default || middleware['rutas']

export default middleware
