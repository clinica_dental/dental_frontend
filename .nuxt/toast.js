import Vue from 'vue'
import Toasted from 'vue-toasted'

Vue.use(Toasted, {"duration":5000,"position":"top-right","keepOnHover":true,"fullWidth":false,"action":{"icon":"close","class":"action-toast","onClick":(e, toastObject) => {
        toastObject.goAway(0);
      }}})

const globals = []
if(globals) {
  globals.forEach(global => {
    Vue.toasted.register(global.name, global.message, global.options)
  })
}

export default function (ctx, inject) {
  inject('toast', Vue.toasted)
}
