# Dental Frontend
 
## Proyecto dental frontend
 
### Instalación
 
> :warning: **Usar node v14 o superior**:
 
Clonar el proyecto desde el repositorio
```bash
git clone https://gitlab.com/proyecto_dental/dental_compilados/dental_frontend.git
cd dental_frontend
```
 
Instalar dependencias
 
```bash
npm i
```
 
Copiar el archivo de configuración donde están definidos puertos, conexiones a la api entre otras configuraciones.
 
```bash
cp nuxt.config.js.sample nuxt.config.js
```
 
Levantar el proyecto
 
```bash
npm run start
```
 
La aplicación se levantará en el puerto definido por defecto **http://localhost:5000**

Las credenciales por defecto son:

**Usuario:** admin

**Password:** 1234567
